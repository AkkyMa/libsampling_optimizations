#include <check.h>
#include <unistd.h>

#include <mol2/atom_group.h>
#include <mol2/pdb.h>
#include <mol2/icharmm.h>
#include "hybridization.h"

struct mol_atom_group *ag;


START_TEST(test_smp_hybridization)
{
	bool has_metadata, read_ok;
	enum smp_hybridization_state *h;

	has_metadata = smp_hybridization_states_present(ag);
	ck_assert(!has_metadata);

	read_ok = smp_hybridization_read(ag, "ala.mol2");
	ck_assert(read_ok);

	has_metadata = smp_hybridization_states_present(ag);
	ck_assert(has_metadata);

	h = smp_hybridization_states_fetch(ag);

	ck_assert(h[0] == SP3_HYBRID);  // N.4
	ck_assert(h[1] == UNKNOWN_HYBRID);  // H
	ck_assert(h[29] == SP2_HYBRID);  // O.co2

	smp_hybridization_states_delete(ag);
	has_metadata = smp_hybridization_states_present(ag);
	ck_assert(!has_metadata);
}
END_TEST


START_TEST(test_smp_hybridization_read_json)
{
	bool read_ok;
	enum smp_hybridization_state *h;

	read_ok = smp_hybridization_read_json(ag, "ala_sybyl.json", true);
	ck_assert(read_ok);

	h = smp_hybridization_states_fetch(ag);

	ck_assert(h[0] == SP3_HYBRID);  // N.4
	ck_assert(h[1] == UNKNOWN_HYBRID);  // H
	ck_assert(h[29] == SP2_HYBRID);  // O.co2

	ck_assert(ag->hbond_prop[0] == HBOND_DONOR);
	ck_assert(ag->hbond_prop[1] == DONATABLE_HYDROGEN);
	ck_assert(ag->hbond_base_atom_id[1] == 0);
	ck_assert(ag->hbond_prop[4] == UNKNOWN_HPROP);
	ck_assert(ag->hbond_prop[7] == HBOND_ACCEPTOR);
	ck_assert(ag->hbond_prop[8] == HBOND_DONOR);
	ck_assert(ag->hbond_prop[9] == DONATABLE_HYDROGEN);
	ck_assert(ag->hbond_base_atom_id[9] == 8);
}
END_TEST


START_TEST(test_smp_hybridization_atom_group_join)
{
	smp_hybridization_read(ag, "ala.mol2");

	struct mol_atom_group *joined = mol_atom_group_join(ag, ag); // Join two copies of the same atomgroup
	ck_assert(!smp_hybridization_states_present(joined));

	smp_hybridization_atom_group_join(joined, ag, ag);
	ck_assert(smp_hybridization_states_present(joined));

	enum smp_hybridization_state *h;
	h = smp_hybridization_states_fetch(joined);

	ck_assert(h[0] == SP3_HYBRID);  // N.4
	ck_assert(h[1] == UNKNOWN_HYBRID);  // H
	ck_assert(h[29] == SP2_HYBRID);  // O.co2
	ck_assert(h[30] == SP3_HYBRID);  // N.4
	ck_assert(h[31] == UNKNOWN_HYBRID);  // H
	ck_assert(h[59] == SP2_HYBRID);  // O.co2

	ck_assert(joined->hbond_prop[0] == HBOND_DONOR);
	ck_assert(joined->hbond_prop[1] == DONATABLE_HYDROGEN);
	ck_assert(joined->hbond_base_atom_id[1] == 0);
	ck_assert(joined->hbond_prop[4] == UNKNOWN_HPROP);
	ck_assert(joined->hbond_prop[7] == HBOND_ACCEPTOR);
	ck_assert(joined->hbond_prop[8] == HBOND_DONOR);
	ck_assert(joined->hbond_prop[9] == DONATABLE_HYDROGEN);
	ck_assert(joined->hbond_base_atom_id[9] == 8);

	ck_assert(joined->hbond_prop[30] == HBOND_DONOR);
	ck_assert(joined->hbond_prop[31] == DONATABLE_HYDROGEN);
	ck_assert(joined->hbond_base_atom_id[31] == 30);
	ck_assert(joined->hbond_prop[34] == UNKNOWN_HPROP);
	ck_assert(joined->hbond_prop[37] == HBOND_ACCEPTOR);
	ck_assert(joined->hbond_prop[38] == HBOND_DONOR);
	ck_assert(joined->hbond_prop[39] == DONATABLE_HYDROGEN);
	ck_assert(joined->hbond_base_atom_id[39] == 38);
}
END_TEST

void setup(void)
{
	ag = mol_read_pdb("ala.pdb");
	mol_atom_group_read_geometry(ag, "ala.psf", "protein.prm", "protein.rtf");
}


void teardown(void)
{
	mol_atom_group_free(ag);
}


Suite *sybyl_suite(void)
{
	Suite *suite = suite_create("sybyl (mol2)");
	TCase *tcase = tcase_create("test_sybyl");
	tcase_add_checked_fixture(tcase, setup, teardown);
	tcase_add_test(tcase, test_smp_hybridization);
	tcase_add_test(tcase, test_smp_hybridization_read_json);
	tcase_add_test(tcase, test_smp_hybridization_atom_group_join);
	suite_add_tcase(suite, tcase);
	return suite;
}


int main(void)
{
	Suite *suite = sybyl_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
