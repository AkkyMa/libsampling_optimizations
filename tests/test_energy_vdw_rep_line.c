#include <stdio.h>
#include <check.h>
#include <math.h>
#include "energy.h"

#include <mol2/pdb.h>
#include <mol2/icharmm.h>

struct mol_atom_group *test_ag;
struct agsetup test_ags;
struct mol_prms *params;

#define ck_assert_double_eq_tol(val, ref, tol) \
	ck_assert(fabs(((val))-((ref))) < (tol));

#define ck_assert_double_eq(val, ref) \
	ck_assert_double_eq_tol(val, ref, 1e-9);

/**
 * Calculates numerical gradient of Van der Waals for single coordinate
 * \param atom_num Atom number
 * \param crd_id Coordinate ID (0 means X, 1 means Y, 2 means Z)
 * \param d Displacement of atom
 * \param weight_rep Parameter for repulsion energy scale
 * \param weight_atr Parameter for attraction energy scale
 * \param energy_rep0 Repulsion energy in unperturbed state
 * \param energy_atr0 Attraction energy in unperturbed state
 * \return Gradient
 */
double calc_energy_vdw_rep_line_grad(size_t atom_num, int crd_id, double d, double weight_rep, double weight_atr, double energy_rep0, double energy_atr0)
{
	double *crd = NULL;
	double energy_rep = 0;
	double energy_atr = 0;

	switch (crd_id) {
		case 0:
			crd = &test_ag->coords[atom_num].X;
			break;
		case 1:
			crd = &test_ag->coords[atom_num].Y;
			break;
		case 2:
			crd = &test_ag->coords[atom_num].Z;
			break;
	}
	double tmp = *crd;
	*crd += d;
	smp_energy_vdw_rep_line(test_ag, &energy_rep, &energy_atr, test_ags.nblst, weight_rep, weight_atr);
	*crd = tmp;
	return ((energy_rep0+energy_atr0)-(energy_rep+energy_atr))/d;
}

/**
 * Calculates numerical gradient of Van der Waals for single coordinate
 * \param atom_num Atom number
 * \param crd_id Coordinate ID (0 means X, 1 means Y, 2 means Z)
 * \param d Displacement of atom
 * \param weight_rep Parameter for repulsion energy scale
 * \param weight_atr Parameter for attraction energy scale
 * \param energy_rep0 Repulsion energy in unperturbed state
 * \param energy_atr0 Attraction energy in unperturbed state
 * \return Gradient
 */
double calc_energy_vdw03_rep_line_grad(size_t atom_num, int crd_id, double d, double weight_rep, double weight_atr, double energy_rep0, double energy_atr0)
{
	double *crd = NULL;
	double energy_rep = 0;
	double energy_atr = 0;

	switch (crd_id) {
		case 0:
			crd = &test_ag->coords[atom_num].X;
			break;
		case 1:
			crd = &test_ag->coords[atom_num].Y;
			break;
		case 2:
			crd = &test_ag->coords[atom_num].Z;
			break;
	}
	double tmp = *crd;
	*crd += d;
	smp_energy_vdw03_rep_line(test_ag, &energy_rep, &energy_atr, test_ags.nblst, test_ags.n03, test_ags.list03,
	                          weight_rep, weight_atr);
	*crd = tmp;
	return ((energy_rep0+energy_atr0)-(energy_rep+energy_atr))/d;
}

double calc_energy_vdw_rep_line_ligand_grad(size_t atom_num, int crd_id, double d, double weight_rep, double weight_atr, double energy0)
{
	double *crd = NULL;
	double energy = 0;

	switch (crd_id) {
		case 0:
			crd = &test_ag->coords[atom_num].X;
			break;
		case 1:
			crd = &test_ag->coords[atom_num].Y;
			break;
		case 2:
			crd = &test_ag->coords[atom_num].Z;
			break;
	}
	double tmp = *crd;
	*crd += d;
	smp_energy_vdw_rep_line_ligand(test_ag, &energy, test_ags.nblst, 10, 20, weight_rep, weight_atr);
	*crd = tmp;
	return (energy0-energy)/d;
}

START_TEST(test_vdw03_rep_line_grads)
{
	double energy_rep = 0.0;
	double energy_atr = 0.0;
	const double weight_rep = 0.05; /**< Default recommended value of repulsion weight, taken from previous MCM code*/
	const double weight_atr = 0.05; /**< Default recommended value of attraction weight, taken from previous MCM code*/
	const double tolerance = 1e-6;
	const double d = 1e-6;
	struct mol_vector3 *fs = calloc(test_ag->natoms, sizeof(struct mol_vector3));

	smp_energy_vdw03_rep_line(test_ag, &energy_rep, &energy_atr, test_ags.nblst, test_ags.n03, test_ags.list03,
	                          weight_rep, weight_atr);

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		fs[i].X = calc_energy_vdw03_rep_line_grad(i, 0, d, weight_rep, weight_atr, energy_rep, energy_atr);
		fs[i].Y = calc_energy_vdw03_rep_line_grad(i, 1, d, weight_rep, weight_atr, energy_rep, energy_atr);
		fs[i].Z = calc_energy_vdw03_rep_line_grad(i, 2, d, weight_rep, weight_atr, energy_rep, energy_atr);
	}

	energy_rep = 0.0;
	energy_atr = 0.0;
	mol_zero_gradients(test_ag);
	smp_energy_vdw03_rep_line(test_ag, &energy_rep, &energy_atr, test_ags.nblst, test_ags.n03, test_ags.list03,
	                          weight_rep, weight_atr);

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		ck_assert_double_eq_tol(test_ag->gradients[i].X, fs[i].X, tolerance);
		ck_assert_double_eq_tol(test_ag->gradients[i].Y, fs[i].Y, tolerance);
		ck_assert_double_eq_tol(test_ag->gradients[i].Z, fs[i].Z, tolerance);
	}
	free(fs);
}
END_TEST

START_TEST(test_vdw_rep_line_grads)
{
	double energy_rep = 0.0;
	double energy_atr = 0.0;

	const double weight_rep = 0.05; /**< Default recommended value of repulsion weight, taken from previous MCM code*/
	const double weight_atr = 0.05; /**< Default recommended value of attraction weight, taken from previous MCM code*/

	const double tolerance = 1e-6;
	const double d = 1e-6;
	struct mol_vector3 *fs = calloc(test_ag->natoms, sizeof(struct mol_vector3));

	smp_energy_vdw_rep_line(test_ag, &energy_rep, &energy_atr, test_ags.nblst, weight_rep, weight_atr);

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		fs[i].X = calc_energy_vdw_rep_line_grad(i, 0, d, weight_rep, weight_atr, energy_rep, energy_atr);
		fs[i].Y = calc_energy_vdw_rep_line_grad(i, 1, d, weight_rep, weight_atr, energy_rep, energy_atr);
		fs[i].Z = calc_energy_vdw_rep_line_grad(i, 2, d, weight_rep, weight_atr, energy_rep, energy_atr);
	}

	energy_rep = 0.0;
	energy_atr = 0.0;
	mol_zero_gradients(test_ag);
	smp_energy_vdw_rep_line(test_ag, &energy_rep, &energy_atr, test_ags.nblst, weight_rep, weight_atr);

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		ck_assert_double_eq_tol(test_ag->gradients[i].X, fs[i].X, tolerance);
		ck_assert_double_eq_tol(test_ag->gradients[i].Y, fs[i].Y, tolerance);
		ck_assert_double_eq_tol(test_ag->gradients[i].Z, fs[i].Z, tolerance);
	}
	free(fs);
}
END_TEST

START_TEST(test_vdw_rep_line_ligand_grads)
{
	double energy = 0;

	const double weight_rep = 1.0;
	const double weight_atr = 1.0;

	const double tolerance = 1e-6;
	const double d = 1e-7; // Use smaller step here, as the test is a bit unstable for large steps
	struct mol_vector3 *fs = calloc(test_ag->natoms, sizeof(struct mol_vector3));

	smp_energy_vdw_rep_line_ligand(test_ag, &energy, test_ags.nblst, 10, 20, weight_rep, weight_atr);

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		fs[i].X = calc_energy_vdw_rep_line_ligand_grad(i, 0, d, weight_rep, weight_atr, energy);
		fs[i].Y = calc_energy_vdw_rep_line_ligand_grad(i, 1, d, weight_rep, weight_atr, energy);
		fs[i].Z = calc_energy_vdw_rep_line_ligand_grad(i, 2, d, weight_rep, weight_atr, energy);
	}

	energy = 0.0;
	mol_zero_gradients(test_ag);
	smp_energy_vdw_rep_line_ligand(test_ag, &energy, test_ags.nblst, 10, 20, weight_rep, weight_atr);

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		ck_assert_double_eq_tol(test_ag->gradients[i].X, fs[i].X, tolerance);
		ck_assert_double_eq_tol(test_ag->gradients[i].Y, fs[i].Y, tolerance);
		ck_assert_double_eq_tol(test_ag->gradients[i].Z, fs[i].Z, tolerance);
	}
	free(fs);
}
END_TEST

START_TEST(test_vdw_rep_line)
{
	double energy_rep = 0.0;
	double energy_atr = 0.0;
	double energy_rep_ref = 0.04951178816;
	double energy_atr_ref = -0.1883820041;
	const double weight_rep = 0.05; /**< Default recommended value of repulsion weight, taken from previous MCM code*/
	const double weight_atr = 0.05; /**< Default recommended value of attraction weight, taken from previous MCM code*/
	smp_energy_vdw_rep_line(test_ag, &energy_rep, &energy_atr, test_ags.nblst, weight_rep, weight_atr);
	ck_assert_double_eq(energy_rep, energy_rep_ref);
	ck_assert_double_eq(energy_atr, energy_atr_ref);
}
END_TEST

START_TEST(test_vdw03_rep_line)
{
	double energy_rep = 0.0;
	double energy_atr = 0.0;
	double energy_rep_ref = 0.1479304756;
	double energy_atr_ref = -0.0960375476;
	const double weight_rep = 0.05; /**< Default recommended value of repulsion weight, taken from previous MCM code*/
	const double weight_atr = 0.05; /**< Default recommended value of attraction weight, taken from previous MCM code*/
	smp_energy_vdw03_rep_line(test_ag, &energy_rep, &energy_atr, test_ags.nblst, test_ags.n03, test_ags.list03,
	                          weight_rep, weight_atr);
	ck_assert_double_eq(energy_rep, energy_rep_ref);
	ck_assert_double_eq(energy_atr, energy_atr_ref);
}
END_TEST

START_TEST(test_vdw_rep_line_ligand)
{
	// We run test with different weight to check that scaling work
	double energy;
	energy = 0;
	smp_energy_vdw_rep_line_ligand(test_ag, &energy, test_ags.nblst, 10, 20, 0.1, 0.0);
	ck_assert_double_eq(energy, -0.2030910690763);
	energy = 0;
	smp_energy_vdw_rep_line_ligand(test_ag, &energy, test_ags.nblst, 10, 20, 0.0, 0.1);
	ck_assert_double_eq(energy, 0.0140969078784);
	energy = 0;
	smp_energy_vdw_rep_line_ligand(test_ag, &energy, test_ags.nblst, 10, 20, 1.0, 1.0);
	ck_assert_double_eq(energy, -1.889941611979);
}
END_TEST

START_TEST(test_vdw_rep_line_pair)
{
	// r_ij = r_i + r_j = 1.6612 + 1.6612 = 3.3224
	// a = 416.528518061529; b = 2894.4565885857614 (see docs)
	// eps_ij = eps_i * eps_j = sqrt(0.21) * sqrt(0.21) = 0.21
	// Clash (r < 0.6 (r_ij) ~ 1.99 A)
	// E = eps_ij * (a + b * (r - C r_ij))
	// G = - eps_ij * b
	// Otherwize (repulsion switches to attraction at 0.89 (r_ij) ~ 2.96 A)
	// E = eps_ij * ((rij/r)^12 - 2 (rij/r)^6 + (rij/rc)^6 (4 - 2 (r/rc)^6) + (rij/rc)^12 (2 (r/rc)^6 - 3)
	// G =
	const double distances[] = {0.0001, 1.9, 2.1, 2.9, 3.3, 3.4, 5.0, 11.9, 12};
	const double energy_rep_ref[] = {1191.0752967127207, 139.20365182683716, 45.056943019147056, 0.12436691153705713, -0.20926149023202337, 0, 0, 0, 0};
	const double energy_atr_ref[] = {0, 0, 0, 0, 0, -0.20610765886321342, -0.03421948819675504, -4.7669187754847654e-07, 0};
	const double force_ref[] = {553.6457944554364, 553.6457944554364, 276.2842371645222, 2.4778917471268325, 0.032943951452806305, -0.08347623308321707, -0.039647719227559684, -9.575794081117099e-06, 0};
	for (int i = 0; i < 9; i++) {
		test_ag->coords[1].X = distances[i];
		double energy_atr = 0.0, energy_rep = 0.0;
		mol_zero_gradients(test_ag);
		smp_energy_vdw_rep_line(test_ag, &energy_rep, &energy_atr, test_ags.nblst, 1.0, 1.0);
		ck_assert_double_eq(energy_rep, energy_rep_ref[i]);
		ck_assert_double_eq(energy_atr, energy_atr_ref[i]);
		ck_assert_double_eq(test_ag->gradients[0].X, -force_ref[i]);
		ck_assert_double_eq(test_ag->gradients[0].Y, 0.0);
		ck_assert_double_eq(test_ag->gradients[0].Z, 0.0);
		ck_assert_double_eq(test_ag->gradients[1].X, force_ref[i]);
		ck_assert_double_eq(test_ag->gradients[1].Y, 0.0);
		ck_assert_double_eq(test_ag->gradients[1].Z, 0.0);
	}
}
END_TEST

void setup_ala(void)
{
	test_ag = mol_read_pdb("ala.pdb");
	mol_atom_group_read_geometry(test_ag, "ala.psf", "protein.prm", "protein.rtf");
	params = mol_prms_read("libmol.params");
	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}

void teardown_ala(void)
{
	mol_atom_group_free(test_ag);
}

void setup_pair(void)
{
	test_ag = mol_read_pdb("two_atoms.pdb");
	mol_atom_group_read_geometry(test_ag, "two_atoms.psf", "small.prm", "small.rtf");
	params = mol_prms_read("libmol.params");
	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}

void teardown_pair(void)
{
	mol_atom_group_free(test_ag);
}

Suite *energy_suite(void)
{
	Suite *suite = suite_create("energy_vdw_repline");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase_ala = tcase_create("test_energy_vdw_rep_line_ala");
	tcase_add_checked_fixture(tcase_ala, setup_ala, teardown_ala);
	tcase_add_test(tcase_ala, test_vdw_rep_line);
	tcase_add_test(tcase_ala, test_vdw_rep_line_grads);

	tcase_add_test(tcase_ala, test_vdw03_rep_line);
	tcase_add_test(tcase_ala, test_vdw03_rep_line_grads);

	tcase_add_test(tcase_ala, test_vdw_rep_line_ligand);
	tcase_add_test(tcase_ala, test_vdw_rep_line_ligand_grads);

	suite_add_tcase(suite, tcase_ala);

	TCase *tcase_pair = tcase_create("test_energy_vdw_rep_line_pair");
	tcase_add_checked_fixture(tcase_pair, setup_pair, teardown_pair);
	tcase_add_test(tcase_pair, test_vdw_rep_line_pair);

	suite_add_tcase(suite, tcase_pair);

	return suite;
}

int main(void)
{
	Suite *suite = energy_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}