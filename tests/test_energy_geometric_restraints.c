#include <stdio.h>
#include <check.h>
#include <math.h>
#include <mol2/atom_group.h>
#include <mol2/pdb.h>
#include <mol2/icharmm.h>
#include "energy.h"


struct mol_atom_group *test_ag;
struct agsetup test_ags;
struct mol_prms *params;

const double tolerance_strict = 1e-9;

#define ck_assert_double_eq_tol(val, ref, tol) \
	ck_assert(fabs(((val))-((ref))) < (tol));

#define ck_assert_double_eq(val, ref) \
	ck_assert_double_eq_tol(val, ref, tolerance_strict);

/**
 * Calculates numerical gradient of geometric restraints potential for single coordinate
 * \param atom_num Atom number
 * \param crd_id Coordinate ID (0 means X, 1 means Y, 2 means Z)
 * \param d Displacement of atom
 * \param en0 Energy in unperturbed state
 * \return Gradient
 */
double calc_restraints_fixedpoint_grad(size_t atom_num, int crd_id, double d, const struct mol_index_list atoms, const struct mol_vector3 *reference_coords, double weight, double en0)
{
	//  for atom atom_num, along coordinate crd_id
	// (0 means X etc)
	double *crd = NULL;
	double en = 0;

	switch (crd_id) {
		case 0:
			crd = &test_ag->coords[atom_num].X;
			break;
		case 1:
			crd = &test_ag->coords[atom_num].Y;
			break;
		case 2:
			crd = &test_ag->coords[atom_num].Z;
			break;
	}
	double tmp = *crd;
	*crd += d;
	smp_energy_restraints_fixedpoint(test_ag, &en, atoms, reference_coords, weight);
	*crd = tmp;
	return (en0 - en) / d;
}

START_TEST(test_smp_energy_restraints_fixedpoint)
{
	double energy;

	struct mol_index_list atoms;
	atoms.size = 3;
	atoms.members = calloc(3, sizeof(size_t));
	atoms.members[0] = 0;
	atoms.members[1] = 7;
	atoms.members[2] = 29;
	struct mol_vector3 reference_coords[3] = {
		{-0.677, -1.230, -0.491}, // Atom #0 is not displaced
		{1.065, -0.922, 0.251}, // Atom #7 is displaced by vector {1, 0, 0} of length 1
		{14.658, 5.357, -2.923}}; // Atom #29 is displaced by vector {-2, -1, -2} of length 3

	const double energy_ref = 10;

	energy = 0;
	mol_zero_gradients(test_ag);
	smp_energy_restraints_fixedpoint(test_ag, &energy, atoms, reference_coords, 1.0);
	ck_assert_double_eq(energy, energy_ref);

	energy = 0;
	mol_zero_gradients(test_ag);
	smp_energy_restraints_fixedpoint(test_ag, &energy, atoms, reference_coords, 0.1);
	ck_assert_double_eq(energy, energy_ref/10);

	// We're test gradients against manually computed ones.
	for (size_t i = 0; i < test_ag->natoms; i++) {
		struct mol_vector3 gradient_ref = { 0 };
		// Most atoms are not restrained, so their gradients are zero.
		// Atom #0 is not displaced, so it's gradient is zero.
		if (i == 7) {
			gradient_ref.X = -2;
		}
		if (i == 29) {
			gradient_ref.X = 4;
			gradient_ref.Y = 2;
			gradient_ref.Z = 4;
		}
		MOL_VEC_MULT_SCALAR(gradient_ref, gradient_ref, 0.1); // Apply energy scaling weight
		ck_assert_double_eq(test_ag->gradients[i].X, gradient_ref.X);
		ck_assert_double_eq(test_ag->gradients[i].Y, gradient_ref.Y);
		ck_assert_double_eq(test_ag->gradients[i].Z, gradient_ref.Z);
	}
}
END_TEST

void setup(void)
{
	test_ag = mol_read_pdb("ala.pdb");
	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
}

void teardown(void)
{
	mol_atom_group_free(test_ag);
}

Suite *energy_suite(void)
{
	Suite *suite = suite_create("smp_energy_restraints_fixedpoint");
	TCase *tcase = tcase_create("test_energy");
	tcase_add_checked_fixture(tcase, setup, teardown);
	tcase_add_test(tcase, test_smp_energy_restraints_fixedpoint);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = energy_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
