/**
 * \file test_prng.c
 * \author Ivan Grebenkin
 * \date 1 Jun 2017
 * \brief File containing test cases and additional functions for testing pseudorandom number generator
 */

#include <stdio.h>
#include <check.h>
#include <math.h>
#include <mol2/vector.h>
#include <mol2/quaternion.h>
#include "transform.h"
#include "prng.h"
#include "utils.h"

#define MOL_QUATERNION_INIT(DST, U, C) do  \
	{                                  \
		(DST).W = C;               \
		(DST).X = (U).X;           \
		(DST).Y = (U).Y;           \
		(DST).Z = (U).Z;           \
	} while(0)

const double tolerance_strict = 1e-9;

#define ck_assert_double_eq_tol(val, ref, tol) \
	ck_assert(fabs(((val))-((ref))) < (tol));

#define ck_assert_double_eq(val, ref) \
	ck_assert_double_eq_tol(val, ref, tolerance_strict);

// Enum to test smp_prng_create validity
enum test_smp_prng_type
{
	TEST_PRNG_TYPE = 1337
};

/* Function for testing uniformity of set of double-precision numbers.
 * The method was used is to compare Chi-Square Probability Function result with certain significance
 * see Book Numerical Recipes: The Art of Scientific Computing, p.221
 *
 * \param distribution_set set of uniformly distributed double-precision numbers
 * \param length of set
 * \param significance chosen significance for verifiable hypothesis
 * \param intervals chosen number of intervals for which the distribution sample is divided
 * \param distribution_parameters unknown number of distribution parameters
 * (depends on the hypothesis that is verified)
 * \return true if hypothesis is valid, false if not
 */
bool _distribution_is_uniform(
	const double *distribution_set,
	const int length,
	const double significance,
	const int intervals,
	const int distribution_parameters);

/* Function for testing normality of set of double-precision numbers.
 * At first it calls _get_uniform_from_normal to extract uniform distribution from normal
 * Then it calls _distribution_is_uniform for extracted set validation
 * see Book Modeling with Itô Stochastic Differential Equations, p.21
 *
 * \param distribution_set set of normally distributed double-precision numbers
 * \param length of set
 * \param significance chosen significance for verifiable hypothesis
 * \param intervals chosen number of intervals for which the distribution sample is divided
 * \param distribution_parameters unknown number of distribution parameters
 * (depends on the hypothesis that is verified)
 * \return true if hypothesis is valid, false if not
 */
bool _distribution_is_gaussian(
	const double *dis_n,
	const int length,
	const double significance,
	const int intervals,
	const int parameters);

/* Integrand of the Gamma function
 * see Book Numerical Recipes: The Art of Scientific Computing, p.213
 *
 * \param t integrand parameter
 * \param power integrand power for parameter
 * \return double-precision value
 */
double _gamma_func(double t, double power);

/* Calculate chi-square statistic value of uniform distribution
 *
 * \param distribution_set set of uniformly distributed double-precision numbers
 * \param length of set
 * \param intervals chosen number of intervals for which the distribution sample is divided
 * \return calculated double-precision chi-square value
 */
double _chi_square_statistic_uniform(const double *distribution_set, const int length, const int intervals);

/* Chi-Square Probability Function
 * see Book Numerical Recipes: The Art of Scientific Computing, p.221
 *
 * \param chi2 shi-square statistic value
 * \param dof degrees of freedom
 * \return double-precision probability value
 */
double _chi_square_probability(double chi2, int dof);

/* Incomplete Gamma Function
 * * see Book Numerical Recipes: The Art of Scientific Computing, p.216
 *
 * \param a degrees of freedom
 * \param x function parameter
 * \return double-precision value of Incomplete Gamma Function
 */
double _gamma_incomplete(double a, double x);

/* Second Simpson's 3/8 rule for numerical integration of Gamma function
 *
 * \param dof degrees of freedom
 * \param a left end point
 * \param b right end point
 * \param N fineness of the partition
 * \return integral value
 */
double _simpsons_38_rule_gamma_func(double dof, double a, double b, int N);

/* Probability density function (pdf) for normal (Gaussian) distribution with parameters \p mean, \p sig
 */
double _normal_density_func(double t, double mean, double sig);

/* Extract uniform distribution from normal distribution
 *
 * \param x value from set of normal distribution
 * \param mean chosen mean
 * \param sig chosen standard deviation
 * \return
 */
double _get_uniform_from_normal(double x, double mean, double sig);


/* _simpsons_38_rule_normal_density_func is a implementation of second Simpson's 3/8 rule
 * for numerical integration of Probability density function
 *
 * \param mean chosen mean
 * \param sig chosen standard deviation
 * \param a left end point
 * \param b right end point
 * \param N fineness of the partition
 * \return integral value
 */
double _simpsons_38_rule_normal_density_func(double mean, double sig, double a, double b, int N);

/* Return calculated mean value for distribution
 */
double _calc_distribution_mean(const double *set, const int length);

/* Return calculated covariance value for distributions
 */
double _calc_dis_covariance(const double *set_1, const double *set_2, const int length);

/* Calculate the quaternion for rotation from v to v0
 */
void _calc_rotatation_quaternion(struct mol_quaternion *q, const struct mol_vector3 *v, const struct mol_vector3 *v0);


START_TEST(test_smp_prng_create)
{
	struct smp_prng *smp = smp_prng_create(42, PRNG_MT19937);
	ck_assert(NULL != smp);
}
END_TEST

START_TEST(test_smp_prng_create_type)
{
	struct smp_prng *smp = smp_prng_create(42, TEST_PRNG_TYPE);
	ck_assert(NULL == smp);
}
END_TEST

START_TEST(test_smp_prng_free)
{
	struct smp_prng *smp = smp_prng_create(42, PRNG_MT19937);
	smp_prng_free(smp);
}
END_TEST

START_TEST(test_smp_prng_destroy)
{
	struct smp_prng *smp = smp_prng_create(42, PRNG_MT19937);
	smp_prng_destroy(smp);
}
END_TEST

/* This scope of testing code tests smp_prng_init in case of MT19937 implementation
 * If another pseudorandom number generator is implemented - it must be refactored
 */
START_TEST(test_smp_prng_init)
{
	struct smp_prng *smp = malloc(sizeof(struct smp_prng));
	ck_assert(NULL != smp);
	smp_prng_init(smp, 42, PRNG_MT19937);
	ck_assert(SMP_PRNG_MT_N == smp->mti);
	ck_assert(42 == smp->mt[0]);
	ck_assert(26719530 == smp->mt[SMP_PRNG_MT_N - 1]);
}
END_TEST

START_TEST(test_smp_prng_uniform)
{
	int length = 10000;
	double dis_u[length];
	struct smp_prng *smp = smp_prng_create(42, PRNG_MT19937);
	for (int i = 0; i < length; ++i) {
		dis_u[i] = smp_prng_uniform(smp);
	}
	int intervals = 10;
	double significance = 0.05;
	int parameters = 0; // unknown distribution parameters, parameters = 0 if a,b are known for uniform distribution
	ck_assert(_distribution_is_uniform(dis_u, length, significance, intervals, parameters));
}
END_TEST

START_TEST(test_smp_prng_uniformf)
{
	int length = 10000;
	double dis_u[length];
	struct smp_prng *smp = smp_prng_create(42, PRNG_MT19937);
	for (int i = 0; i < length; ++i) {
		dis_u[i] = smp_prng_uniformf(smp);
	}
	int intervals = 10;
	double significance = 0.05;
	int parameters = 0; // number of unknown distribution parameters, parameters = 0 if a,b are known for uniform distribution
	ck_assert(_distribution_is_uniform(dis_u, length, significance, intervals, parameters));
}
END_TEST

START_TEST(test_smp_prng_gaussian)
{
	int length = 10000;
	double dis_n[length];
	struct smp_prng *smp = smp_prng_create(42, PRNG_MT19937);
	for (int i = 0; i < length; ++i) {
		dis_n[i] = smp_prng_gaussian(smp);
	}
	int intervals = 10;
	double significance = 0.05;
	int parameters = 0; // unknown distribution parameters, parameters = 0 if a,b are known for uniform distribution
	ck_assert(_distribution_is_gaussian(dis_n, length, significance, intervals, parameters));
}
END_TEST

START_TEST(test_smp_prng_gaussianf)
{
	int length = 10000;
	double dis_n[length];
	struct smp_prng *smp = smp_prng_create(42, PRNG_MT19937);
	for (int i = 0; i < length; ++i) {
		dis_n[i] = smp_prng_gaussianf(smp);
	}

	int intervals = 10;
	double significance = 0.05;
	int parameters = 0; // unknown distribution parameters, parameters = 0 if a,b are known for uniform distribution
	ck_assert(_distribution_is_gaussian(dis_n, length, significance, intervals, parameters));
}
END_TEST

START_TEST(test_smp_prng_gaussian2)
{
	int length = 10000;
	double dis_n_1[length];
	double dis_n_2[length];
	struct smp_prng *smp = smp_prng_create(42, PRNG_MT19937);
	for (int i = 0; i < length; ++i) {
		smp_prng_gaussian2(smp, &dis_n_1[i], &dis_n_2[i]);
	}

	int intervals = 10;
	double significance = 0.05;
	int parameters = 0; // unknown distribution parameters, parameters = 0 if a,b are known for uniform distribution

	ck_assert(_distribution_is_gaussian(dis_n_1, length, significance, intervals, parameters));
	ck_assert(_distribution_is_gaussian(dis_n_2, length, significance, intervals, parameters));

	double cov_1 = _calc_dis_covariance(dis_n_1, dis_n_1, length);
	double cov_2 = _calc_dis_covariance(dis_n_2, dis_n_2, length);
	double cov_12 = _calc_dis_covariance(dis_n_1, dis_n_2, length);
	double cov_21 = _calc_dis_covariance(dis_n_2, dis_n_1, length);
	ck_assert((cov_12 == cov_21) && (fabs(floor(cov_12 * 10. + .5) / 10.) == 0.));
	ck_assert((floor(cov_1) == floor(cov_2)) && (fabs(floor(cov_1 * 10. + .5) / 10.) == 1.));
}
END_TEST

START_TEST(test_smp_prng_gaussian2f)
{
	int length = 10000;
	double dis_n_1[length];
	double dis_n_2[length];
	float float_1, float_2;
	struct smp_prng *smp = smp_prng_create(42, PRNG_MT19937);
	for (int i = 0; i < length; ++i) {
		smp_prng_gaussian2f(smp, &float_1, &float_2);
		dis_n_1[i] = float_1;
		dis_n_2[i] = float_2;
	}

	int intervals = 10;
	double significance = 0.05;
	int parameters = 0; // unknown distribution parameters, parameters = 0 if a,b are known for uniform distribution

	ck_assert(_distribution_is_gaussian(dis_n_1, length, significance, intervals, parameters));
	ck_assert(_distribution_is_gaussian(dis_n_2, length, significance, intervals, parameters));

	double cov_1 = _calc_dis_covariance(dis_n_1, dis_n_1, length);
	double cov_2 = _calc_dis_covariance(dis_n_2, dis_n_2, length);
	double cov_12 = _calc_dis_covariance(dis_n_1, dis_n_2, length);
	double cov_21 = _calc_dis_covariance(dis_n_2, dis_n_1, length);
	ck_assert((cov_12 == cov_21) && (fabs(floor(cov_12 * 10. + .5) / 10.) == 0.));
	ck_assert((floor(cov_1) == floor(cov_2)) && (fabs(floor(cov_1 * 10. + .5) / 10.) == 1.));
}
END_TEST

/** We applying quaternions generated by \c smp_prng_quaternion to vector \p v0.
 * The resulting distribution should be uniform along all axis.
 */
void _test_prng_quaternion_for_vector(const struct mol_vector3 v0)
{
	static const int length = 10000;
	static const int intervals = 10;
	static const double significance = 0.05;
	static const int parameters = 0; // number of unknown distribution parameters, parameters = 0 if a,b are known for uniform distribution
	const double v0_norm = sqrt(MOL_VEC_SQ_NORM(v0));

	double *dis_ux = calloc(length, sizeof(double));
	double *dis_uy = calloc(length, sizeof(double));
	double *dis_uz = calloc(length, sizeof(double));

	struct smp_prng *prng = smp_prng_create(1337, PRNG_MT19937);

	for (int i = 0; i < length; i++) {
		struct mol_vector3 v;
		struct mol_quaternion q;

		smp_prng_quaternion(prng, &q);
		MOL_VEC_COPY(v, v0);
		smp_vector3_rotate_by_quaternion(&v, &q);

		dis_ux[i] = (v.X + v0_norm) / (2 * v0_norm);
		dis_uy[i] = (v.Y + v0_norm) / (2 * v0_norm);
		dis_uz[i] = (v.Z + v0_norm) / (2 * v0_norm);
	}

	ck_assert(_distribution_is_uniform(dis_ux, length, significance, intervals, parameters));
	ck_assert(_distribution_is_uniform(dis_uy, length, significance, intervals, parameters));
	ck_assert(_distribution_is_uniform(dis_uz, length, significance, intervals, parameters));
}

/** In this test, we rotate given vector by small amount.
 * Then we apply the rotation transformation from v0 to OZ.
 * This way, the resulting vector should be distributed in the small sector around OZ.
 * We check that the angle of rotation around OZ (theta) in uniformly distributed on [-pi; pi].
 * We check that the angle of rotation between OZ and result (alpha) does not exceed delta*pi.
 * We check that we sometimes have at least small and large values of alpha, even though we don't thoroughly test the distribution.
 */
void _test_prng_quaternion_small_for_vector(const struct mol_vector3 v0)
{
	const double v0_norm = sqrt(MOL_VEC_SQ_NORM(v0));
	struct mol_vector3 v_base = {.X = 0, .Y = 0, .Z = 1}; // "Base" vector
	struct mol_quaternion q_base;
	_calc_rotatation_quaternion(&q_base, &v0, &v_base);  // We calculate the rotation from v0 to "base"

	static const double delta_start = 0.1;
	static const double delta_step = .1;
	static const double delta_end = 1;

	static const double alpha_eps = 0.95;
	static const int length = 10000;
	static const int intervals = 10;
	static const double significance = 0.05;
	static const int parameters = 0; // number of unknown distribution parameters, parameters = 0 if a,b are known for uniform distribution

	double *dis_u = calloc(length, sizeof(double));
	struct smp_prng *prng = smp_prng_create(42, PRNG_MT19937);

	for (double delta = delta_start; delta <= delta_end; delta += delta_step) {
		const double alpha_max = delta * M_PI;
		bool has_big_alpha = false;
		bool has_small_alpha = false;
		for (int i = 0; i < length; i++) {
			struct mol_quaternion q;
			struct mol_vector3 v;

			smp_prng_quaternion_small(prng, &q, delta);
			MOL_VEC_COPY(v, v0);
			smp_vector3_rotate_by_quaternion(&v, &q);
			// We rotate the result so it should be around "base" vector
			smp_vector3_rotate_by_quaternion(&v, &q_base);
			MOL_VEC_DIV_SCALAR(v, v, v0_norm);

			double theta = acos(v.X / sqrt(1 - pow(v.Z, 2))); // Rotation around "base"
			if (v.Y < 0) theta *= -1;
			dis_u[i] = (theta + M_PI) / (2 * M_PI);

			const double alpha = acos(v.Z); // Rotation between result and "base"
			ck_assert(alpha <= alpha_max);
			if (alpha >= alpha_max * alpha_eps)
				has_big_alpha = true;
			if (alpha < alpha_max * (1 - alpha_eps))
				has_small_alpha = true;
		}
		ck_assert(_distribution_is_uniform(dis_u, length, significance, intervals, parameters));
		ck_assert(has_big_alpha);
		ck_assert(has_small_alpha);
	}
}


/** In this test, we check that at delta = 1 the behavior of smp_prng_quaternion_small mathes the
 * behavior of smp_prng_quaternion.
*/
void _test_prng_quaternion_small_delta_1()
{
	static const int length = 10000;
	static const unsigned long seed = 1337;
	struct smp_prng *prng_1 = smp_prng_create(seed, PRNG_MT19937);
	struct smp_prng *prng_2 = smp_prng_create(seed, PRNG_MT19937);

	for (int i = 0; i < length; i++) {
		struct mol_quaternion q_1, q_2;

		smp_prng_quaternion_small(prng_1, &q_1, 1.0);
		smp_prng_quaternion(prng_2, &q_2);

		// smp_prng_quaternion_small sometimes multiplies the result by -1
		// This corresponds to the same rotation, so we just undo it if we suspect something it
		if (q_1.W * q_2.W < 0)
			SMP_QUATERNION_MULT_SCALAR(q_1, q_1, -1);

		ck_assert_double_eq(q_1.W, q_2.W);
		ck_assert_double_eq(q_1.X, q_2.X);
		ck_assert_double_eq(q_1.Y, q_2.Y);
		ck_assert_double_eq(q_1.Z, q_2.Z);
	}
}

/** In this test, we check that at delta = 0 we always produce unit quaternions
*/
void _test_prng_quaternion_small_delta_0()
{
	static const int length = 10;
	struct smp_prng *prng = smp_prng_create(1337, PRNG_MT19937);

	for (int i = 0; i < length; i++) {
		struct mol_quaternion q;

		smp_prng_quaternion_small(prng, &q, 0.0);
		ck_assert_double_eq(q.W, 1);
		ck_assert_double_eq(q.X, 0);
		ck_assert_double_eq(q.Y, 0);
		ck_assert_double_eq(q.Z, 0);
	}
}

START_TEST(test_smp_prng_quaternion)
{
	// See \ref _test_prng_quaternion_for_vector for description of the test
	struct mol_vector3 v1 = {.X = 1, .Y = 0, .Z = 0};
	_test_prng_quaternion_for_vector(v1);

	struct mol_vector3 v2 = {.X = 1, .Y = 2, .Z = 2};
	_test_prng_quaternion_for_vector(v2);

	struct mol_vector3 v3 = {.X = (M_PI/6.0) * (1/sqrt(3)), .Y = (M_PI/6.0) * (1/sqrt(3)), .Z = (M_PI/6.0) * (1/sqrt(3))};
	_test_prng_quaternion_for_vector(v3);

}
END_TEST

START_TEST(test_smp_prng_quaternion_small)
{
	// See _test_prng_quaternion_small_for_vector and _test_prng_quaternion_small_delta_1
	struct mol_vector3 v0 = {.X = 0, .Y = 0, .Z = 1};
	_test_prng_quaternion_small_for_vector(v0);

	struct mol_vector3 v1 = {.X = 0, .Y = 1, .Z = 0};
	_test_prng_quaternion_small_for_vector(v1);

	struct mol_vector3 v2 = {.X = 1, .Y = 2, .Z = 2};
	_test_prng_quaternion_small_for_vector(v2);

	struct mol_vector3 v3 = {.X = 1./3, .Y = 2./3, .Z = 2./3};
	_test_prng_quaternion_small_for_vector(v3);

	struct mol_vector3 v4 = {.X = (M_PI/6.0) * (1/sqrt(3)), .Y = (M_PI/6.0) * (1/sqrt(3)), .Z = (M_PI/6.0) * (1/sqrt(3))};
	_test_prng_quaternion_small_for_vector(v4);

	_test_prng_quaternion_small_delta_0();
	_test_prng_quaternion_small_delta_1();
}
END_TEST


void _test_calc_rotatation_quaternion_for_vectors(struct mol_vector3 *v0, const struct mol_vector3 *vb)
{
	struct mol_quaternion q;
	_calc_rotatation_quaternion(&q, v0, vb);
	const double v_norm = sqrt(MOL_VEC_SQ_NORM(*v0));
	smp_vector3_rotate_by_quaternion(v0, &q);
	MOL_VEC_DIV_SCALAR(*v0, *v0, v_norm);
	ck_assert_double_eq(vb->X, v0->X);
	ck_assert_double_eq(vb->Y, v0->Y);
	ck_assert_double_eq(vb->Z, v0->Z);
}

// This is kind of a meta-test that checks the internal _calc_rotatation_quaternion function
START_TEST(test_calc_rotatation_quaternion)
{
	struct mol_vector3 v1 = {.X = 0, .Y = 1, .Z = 0};
	struct mol_vector3 v2 = {.X = 1, .Y = 0, .Z = 0};
	struct mol_vector3 v3 = {.X = 1, .Y = 2, .Z = 2};
	struct mol_vector3 v4 = {.X = (M_PI/6.0) * (1/sqrt(3)), .Y = (M_PI/6.0) * (1/sqrt(3)), .Z = (M_PI/6.0) * (1/sqrt(3))};
	struct mol_vector3 vb = {.X = 0, .Y = 0, .Z = 1};
	_test_calc_rotatation_quaternion_for_vectors(&v1, &vb);
	_test_calc_rotatation_quaternion_for_vectors(&v2, &vb);
	_test_calc_rotatation_quaternion_for_vectors(&v3, &vb);
	_test_calc_rotatation_quaternion_for_vectors(&v4, &vb);
}
END_TEST

Suite *featurex_suite(void) {
	Suite *suite = suite_create("prng_functions");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test_prng_functions");
	tcase_set_timeout(tcase, 3000);
	// Self-test
	tcase_add_test(tcase, test_calc_rotatation_quaternion);

	// Constructor-destructor tests
	tcase_add_test(tcase, test_smp_prng_create);
	tcase_add_test(tcase, test_smp_prng_create_type);
	tcase_add_test(tcase, test_smp_prng_free);
	tcase_add_test(tcase, test_smp_prng_destroy);
	tcase_add_test(tcase, test_smp_prng_init);

	// Functional tests
	tcase_add_test(tcase, test_smp_prng_uniform);
	tcase_add_test(tcase, test_smp_prng_uniformf);
	tcase_add_test(tcase, test_smp_prng_gaussian);
	tcase_add_test(tcase, test_smp_prng_gaussianf);
	tcase_add_test(tcase, test_smp_prng_gaussian2);
	tcase_add_test(tcase, test_smp_prng_gaussian2f);
	tcase_add_test(tcase, test_smp_prng_quaternion);
	tcase_add_test(tcase, test_smp_prng_quaternion_small);

	suite_add_tcase(suite, tcase);

	return suite;
}



int main(void)
{
	Suite *suite = featurex_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}

bool _distribution_is_uniform(
		const double *distribution_set,
		const int length,
		const double significance,
		const int intervals,
		const int distribution_parameters)
{
	int dof = intervals - distribution_parameters;
	for (int i = 0; i < length; i++) { // Sanity check
		if (distribution_set[i] < 0 || distribution_set[i] >= 1 || !isfinite(distribution_set[i]))
			return false;
	}
	double chi2 = _chi_square_statistic_uniform(distribution_set, length, dof);
	double probability = _chi_square_probability(chi2, dof);
	return probability > significance;
}


bool _distribution_is_gaussian(
		const double *dis_n,
		const int length,
		const double significance,
		const int intervals,
		const int parameters)
{
	double dis_u[length];
	for (int i = 0; i < length; ++i) {
		dis_u[i] = _get_uniform_from_normal(dis_n[i], 0, 1);
	}
	return _distribution_is_uniform(dis_u, length, significance, intervals, parameters);
}

double _chi_square_statistic_uniform(const double *distribution_set, const int length, const int intervals)
{
	double prob_u = (double) (length / intervals);
	double frequencies[intervals];
	for (int j = 0; j < intervals; ++j) {
		frequencies[j] = 0;
	}
	for (int k = 0; k < length; ++k) {
		frequencies[(int) (distribution_set[k] * intervals)]++;
	}
	double chi2 = 0;
	for (int l = 0; l < intervals; ++l) {
		chi2 += ((frequencies[l] - prob_u) * (frequencies[l] - prob_u)) / prob_u;
	}
	return chi2;
}

double _chi_square_probability(double chi2, int dof)
{
	return _gamma_incomplete(0.5 * dof, 0.5 * chi2);
}

double _gamma_incomplete(double a, double x)
{
	double h = 1.15e-3;
	return 1.0 - _simpsons_38_rule_gamma_func(a - 1, 0., x, (int) (x / h)) / tgamma(a);
}

double _simpsons_38_rule_gamma_func(double dof, double a, double b, int N)
{
	double h = (b - a) / N;
	double sum = _gamma_func(a, dof) + _gamma_func(b, dof);
	for (int i = N - 1; i > 0; i--) {
		sum += ((i % 3) ? 3.0 : 2.0) * _gamma_func(a + h * i, dof);
	}
	return (3.0 / 8.0) * h * sum;
}

double _gamma_func(double t, double power)
{
	return pow(t, power) * exp(-t);
}

double _normal_density_func(double t, double mean, double sig)
{
	return (1 / sqrt(2 * M_PI * pow(sig, 2))) * exp(-(pow(t - mean, 2)) / (2 * pow(sig, 2)));
}

double _get_uniform_from_normal(double x, double mean, double sig)
{
	double h = 1.15e-3;
	if (x > mean) {
		return 0.5 + _simpsons_38_rule_normal_density_func(mean, sig, mean, x, (int) (fabs(x - mean) / h));
	} else if (x < mean) {
		return 0.5 - _simpsons_38_rule_normal_density_func(mean, sig, x, mean, (int) (fabs(x - mean) / h));
	} else {
		return 0.5;
	}
}

double _simpsons_38_rule_normal_density_func(double mean, double sig, double a, double b, int N)
{
	if (N < 50) {
		N = 50;
	}
	double h = (b - a) / N;
	double sum = _normal_density_func(a, mean, sig) + _normal_density_func(b, mean, sig);
	for (int i = N - 1; i > 0; i--) {
		sum += ((i % 3) ? 3.0 : 2.0) * _normal_density_func(a + h * i, mean, sig);
	}
	return (3.0 / 8.0) * h * sum;
}

double _calc_distribution_mean(const double *set, const int length)
{
	double sum = 0;
	for (int i = 0; i < length; ++i) {
		sum += set[i];
	}
	return sum / (double) length;
}

double _calc_dis_covariance(const double *set_1, const double *set_2, const int length)
{
	double mean_1 = _calc_distribution_mean(set_1, length);
	double mean_2 = _calc_distribution_mean(set_2, length);
	double sum = 0;
	for (int i = 0; i < length; ++i) {
		sum += (set_1[i] - mean_1) * (set_2[i] - mean_2);
	}
	return sum / (double) (length - 1);
}


void _calc_rotatation_quaternion(struct mol_quaternion *q, const struct mol_vector3 *v, const struct mol_vector3 *v0)
{
	struct mol_vector3 crossprod;
	MOL_VEC_CROSS_PROD(crossprod, *v, *v0);
	double w = sqrt(MOL_VEC_SQ_NORM(*v0)*MOL_VEC_SQ_NORM(*v)) + MOL_VEC_DOT_PROD(*v0, *v);
	MOL_QUATERNION_INIT(*q, crossprod, w);
	*q = mol_quaternion_normalize(*q);
}
