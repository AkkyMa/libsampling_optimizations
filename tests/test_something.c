#include <stdio.h>
#include <check.h>
#include <errno.h>

#include "myfile.h"
#include "mol2/atom_group.h"
#include "mol2/pdb.h"

const double delta = 0.000001;
const double tolerance = 1e-3;
const double tolerance_strict = 1e-9;

// Test cases
START_TEST(test_myfunc)
{
	int myval = smp_object_action();
	ck_assert(myval == 42);
}
END_TEST

START_TEST(test_myfunc2)
{
	struct mol_atom_group* ag = mol_read_pdb("1rei.pdb");
	ck_assert(ag != NULL);
	int myval = smp_object_action2(ag);
	ck_assert_int_eq(myval, 1707);
}
END_TEST

Suite *featurex_suite(void)
{
	Suite *suite = suite_create("featurex");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.
	TCase *tcase = tcase_create("test_featurex");
	tcase_add_test(tcase, test_myfunc);
	tcase_add_test(tcase, test_myfunc2);

	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = featurex_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}

