#include <stdio.h>
#include <check.h>
#include <math.h>
#include "energy.h"

#include <mol2/pdb.h>
#include <mol2/icharmm.h>

struct mol_atom_group *test_ag;
struct agsetup test_ags;
struct mol_prms *params;

#define ck_assert_double_eq_tol(val, ref, tol) \
	ck_assert(fabs(((val))-((ref))) < (tol));

#define ck_assert_double_eq(val, ref) \
	ck_assert_double_eq_tol(val, ref, 1e-9);

/**
 * \ref Calculates numerical gradient of Coulomb for single coordinate.
 * \param atom_num Atom number
 * \param crd_id Coordinate ID (0 means X, 1 means Y, 2 means Z)
 * \param d Displacement of atom
 * \param weight_short Parameter for short energy function
 * \param weight_long Parameter for long energy function
 * \param energy_short0 Short energy in unperturbed state
 * \param energy_long0 Long energy in unperturbed state
 * \return Gradient
 */
double calc_coul_rdie_shift_grad(size_t atom_num, int crd_id, double d, const double weight_short, const double weight_long, double energy_short0, double energy_long0)
{
	double *crd = NULL;
	double energy_short = 0;
	double energy_long = 0;

	switch (crd_id) {
		case 0:
			crd = &test_ag->coords[atom_num].X;
			break;
		case 1:
			crd = &test_ag->coords[atom_num].Y;
			break;
		case 2:
			crd = &test_ag->coords[atom_num].Z;
			break;
	}
	double tmp = *crd;
	*crd += d;
	smp_energy_coul_rdie_shift(test_ag, &energy_short, &energy_long, test_ags.nblst, weight_short, weight_long);
	*crd = tmp;
	return ((energy_short0+energy_long0)-(energy_short+energy_long))/d;
}

START_TEST(test_energy_coul_rdie_shift)
{
	const double default_w_coul_short = 0.025; /**< Default recommended value of weight, taken from previous MCM code*/
	const double default_w_coul_long = 0.025; /**< Default recommended value of weight, taken from previous MCM code*/
	double energy_sht = 0;
	double energy_lng = 0;
	double energy_sht_ref = -1.165811879350;
	double energy_lng_ref = -0.066515577275;
	smp_energy_coul_rdie_shift(test_ag, &energy_sht, &energy_lng, test_ags.nblst, default_w_coul_short,
	                           default_w_coul_long);
	ck_assert_double_eq(energy_sht, energy_sht_ref);
	ck_assert_double_eq(energy_lng, energy_lng_ref);
}
END_TEST

START_TEST(test_energy_coul_rdie_shift_grads)
{
	const double default_w_coul_short = 0.025; /**< Default recommended value of weight, taken from previous MCM code*/
	const double default_w_coul_long = 0.025; /**< Default recommended value of weight, taken from previous MCM code*/
	double energy_sht = 0;
	double energy_lng = 0;
	double tolerance = 1e-6;
	double d = 1e-6;
	struct mol_vector3 *fs = calloc(test_ag->natoms, sizeof(struct mol_vector3));

	smp_energy_coul_rdie_shift(test_ag, &energy_sht, &energy_lng, test_ags.nblst, default_w_coul_short,
	                           default_w_coul_long);

	for (size_t i = 0; i < test_ag->natoms; ++i) {
		fs[i].X = calc_coul_rdie_shift_grad(i, 0, d, default_w_coul_short, default_w_coul_long, energy_sht,
		                                    energy_lng);
		fs[i].Y = calc_coul_rdie_shift_grad(i, 1, d, default_w_coul_short, default_w_coul_long, energy_sht,
		                                    energy_lng);
		fs[i].Z = calc_coul_rdie_shift_grad(i, 2, d, default_w_coul_short, default_w_coul_long, energy_sht,
		                                    energy_lng);
	}
	energy_sht = 0.0;
	energy_lng = 0.0;
	mol_zero_gradients(test_ag);
	smp_energy_coul_rdie_shift(test_ag, &energy_sht, &energy_lng, test_ags.nblst, default_w_coul_short,
	                           default_w_coul_long);
	for (size_t i = 0; i < test_ag->natoms; ++i) {
		ck_assert_double_eq_tol(test_ag->gradients[i].X, fs[i].X, tolerance);
		ck_assert_double_eq_tol(test_ag->gradients[i].Y, fs[i].Y, tolerance);
		ck_assert_double_eq_tol(test_ag->gradients[i].Z, fs[i].Z, tolerance);
	}
	free(fs);
}
END_TEST

START_TEST(test_energy_coul_rdie_shift_pair)
{
	// E = C_elec * q1 * q2 * (1 - r/rc)^2 / r^2
	// G = C_elec * q1 * q2 * 2 * (1/r^3 - 1/(rc*r^2)) * r/|r|
	const double distances[] = {0.0001, 2.9, 3.1, 4.9, 5.1, 12, 200};
	const double energy_sht_ref[] = {20.754475, 20.754475, 19.007538036189153, 4.841654332685452, 0, 0, 0};
	const double energy_lng_ref[] = {0, 0, 0, 0, 4.221113908112264, 0, 0};
	const double force_ref[] = {0, 0, 16.534284627348303, 3.340031732809741, 2.8788500652087055, 0, 0};
	for (int i = 0; i < 7; i++) {
		test_ag->coords[1].X = distances[i];
		double energy_sht = 0.0, energy_lng = 0.0;
		mol_zero_gradients(test_ag);
		smp_energy_coul_rdie_shift(test_ag, &energy_sht, &energy_lng, test_ags.nblst, 1.0, 1.0);
		ck_assert_double_eq(energy_sht, energy_sht_ref[i]);
		ck_assert_double_eq(energy_lng, energy_lng_ref[i]);
		ck_assert_double_eq(test_ag->gradients[0].X, -force_ref[i]);
		ck_assert_double_eq(test_ag->gradients[0].Y, 0.0);
		ck_assert_double_eq(test_ag->gradients[0].Z, 0.0);
		ck_assert_double_eq(test_ag->gradients[1].X, force_ref[i]);
		ck_assert_double_eq(test_ag->gradients[1].Y, 0.0);
		ck_assert_double_eq(test_ag->gradients[1].Z, 0.0);
	}
}
END_TEST

void setup_ala(void)
{
	test_ag = mol_read_pdb("ala.pdb");
	mol_atom_group_read_geometry(test_ag, "ala.psf", "protein.prm", "protein.rtf");
	params = mol_prms_read("libmol.params");
	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}

void teardown_ala(void)
{
	mol_atom_group_free(test_ag);
}

void setup_pair(void)
{
	test_ag = mol_read_pdb("two_atoms.pdb");
	mol_atom_group_read_geometry(test_ag, "two_atoms.psf", "small.prm", "small.rtf");
	params = mol_prms_read("libmol.params");
	test_ag->gradients = calloc(test_ag->natoms, sizeof(struct mol_vector3));
	mol_fixed_init(test_ag);
	mol_fixed_update(test_ag, 0, NULL); // Make nothing fixed
	init_nblst(test_ag, &test_ags);
	update_nblst(test_ag, &test_ags);
}

void teardown_pair(void)
{
	mol_atom_group_free(test_ag);
}

Suite *energy_suite(void)
{
	Suite *suite = suite_create("energy_coul_rdie_shift");

	// Add test cases here
	// Each test case can call multiple test functions
	// Too add a test case, call tcase_add_test
	// The first argument is the TCase struct, the second is the
	//  test function name.

	TCase *tcase_ala = tcase_create("test_energy_ala");
	tcase_add_checked_fixture(tcase_ala, setup_ala, teardown_ala);
	tcase_add_test(tcase_ala, test_energy_coul_rdie_shift);
	tcase_add_test(tcase_ala, test_energy_coul_rdie_shift_grads);
	suite_add_tcase(suite, tcase_ala);


	TCase *tcase_pair = tcase_create("test_energy_pair");
	tcase_add_checked_fixture(tcase_pair, setup_pair, teardown_pair);
	tcase_add_test(tcase_pair, test_energy_coul_rdie_shift_pair);
	suite_add_tcase(suite, tcase_pair);

	return suite;
}

int main(void)
{
	Suite *suite = energy_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}