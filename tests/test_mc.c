#include <string.h>
#include <check.h>
#include <math.h>

#include "../src/mc.h"
#include "../src/prng.h"


START_TEST(test_metropolis)
{
	struct smp_prng *prng = smp_prng_create(1, PRNG_MT19937);

	// If new energy is less than old, always accept
	for (size_t i = 0; i < 100; i++)
		ck_assert(smp_mc_metropolis(prng, 0.0, -1.0, 1.0) == true);

	// If new energy is much greater than old, effectively always reject
	for (size_t i = 0; i < 100; i++)
		ck_assert(smp_mc_metropolis(prng, 0.0, 1.0e8, 1.0) == false);

	// Probability of acceptance is exp(-1)
	size_t x = 0;
	const size_t n_trials = 100;
	for (size_t i = 0; i < n_trials; i++)
		x += smp_mc_metropolis(prng, 0.0, 2.0, 2.0);
	// Check that the obtained value lies in 95% confidence interval.
	// We use Agresti-Coull approximation for the confidence interval.
	const double z = 1.96; // (1-0.05/2) quantile of normal distribution
	const double n = n_trials + z*z;
	const double p = (x + z*z/2) / n;
	const double dp = z * sqrt(p*(1-p)/n);
	ck_assert(x >= (p - dp)*n_trials);
	ck_assert(x <= (p + dp)*n_trials);
}
END_TEST


Suite *mc_suite(void)
{
	Suite *suite = suite_create("mc");
	TCase *tcase = tcase_create("test_mc");
	tcase_add_test(tcase, test_metropolis);
	suite_add_tcase(suite, tcase);

	return suite;
}

int main(void)
{
	Suite *suite = mc_suite();
	SRunner *runner = srunner_create(suite);
	srunner_run_all(runner, CK_ENV);

	int number_failed = srunner_ntests_failed(runner);
	srunner_free(runner);
	return number_failed;
}
