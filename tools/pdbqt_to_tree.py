#!/usr/bin/env python3

import argparse
import collections
import sys
try:
    import typing
    from typing import Optional, List, Dict
except ImportError:
    # Python 2 does not support type hints. Whatever.
    pass


Tree = collections.namedtuple('Tree', ['nodes', 'edges'])


def _is_atom(pdb_line):
    # type: (str) -> bool
    prefix = pdb_line[:6]
    return prefix in ['ATOM  ', 'HETATM']


def _get_atom_num(pdb_line):
    # type : (str) -> int
    if not _is_atom(pdb_line):
        raise ValueError('Can not get atom number from non-ATOM PDB line')
    return int(pdb_line[6:11])


def _get_atom_coord(pdb_line):
    # type : (str) -> str
    if not _is_atom(pdb_line):
        raise ValueError('Can not get atom number from non-ATOM PDB line')
    return pdb_line[30:54]


def _map_crd_to_num(pdb_data):
    # type: (List[str]) -> Dict[str, int]
    return {
        _get_atom_coord(l): _get_atom_num(l)
        for l in pdb_data
        if _is_atom(l)
    }


def _get_num_active_torsions(remarks):
    # type: (List[str]) -> int
    for remark in remarks:
        if 'active torsions' in remark:
            return int(remark.split()[1])
    return 0


def tree_from_pdbqt(pdbqt_data):
    # type: (List[str]) -> Tree
    remarks = [l for l in pdbqt_data if l.startswith('REMARK')]
    n_nodes = _get_num_active_torsions(remarks) + 1
    nodes = []
    edges = []
    current_node = []
    for line in pdbqt_data:
        try:
            prefix, rest = line.split(None, 1)
        except ValueError:
            prefix, rest = line.strip(), ''
        if prefix in ['ROOT', 'ENDROOT', 'BRANCH', 'ENDBRANCH']:
            if len(current_node) > 0:
                nodes.append(current_node)
                current_node = []
        if prefix in ['ATOM', 'HETATM']:
            current_node.append(_get_atom_num(line))
        if prefix == 'ENDBRANCH':
            i, j = map(int, rest.split())
            edges.append((i, j))
    if len(nodes) != n_nodes:
        raise ValueError('Unable to parse PDBQT data')
    if len(edges) != n_nodes - 1:
        raise ValueError('Unable to parse PDBQT data')
    return Tree(
        nodes=nodes,
        edges=edges
    )


def map_pdbqt_to_pdb(pdb1_data, pdb2_data):
    # type: (List[str], List[str]) -> Dict[int, int]
    pdb1_crd_data = _map_crd_to_num(pdb1_data)
    pdb2_crd_data = _map_crd_to_num(pdb2_data)

    if len(pdb1_crd_data.keys()) != len(pdb2_crd_data.keys()):
        raise ValueError('Different number of atoms in two files')
    if set(pdb1_crd_data.keys()) != set(pdb2_crd_data.keys()):
        raise ValueError('Unable to match coordinates')

    return {pdb1_crd_data[v]: pdb2_crd_data[v] for v in pdb1_crd_data.keys()}


def build_tree(pdbqt_data, pdb_data, shift):
    # type: (List[str], List[str], int) -> Tree
    tree = tree_from_pdbqt(pdbqt_data)
    mapping = map_pdbqt_to_pdb(pdbqt_data, pdb_data)
    for k in mapping.keys():
        mapping[k] += shift
    return Tree(
        nodes=[
            sorted([mapping[atom] for atom in node])
            for node in tree.nodes
        ],
        edges=[
            (mapping[edge[0]], mapping[edge[1]])
            for edge in tree.edges
        ]
    )


def write_free_atoms(fp):
    # type: (typing.TextIO) -> None
    """
    Write "free atoms" section. So far, it is empty.
    :param fp: Output file descriptor
    """
    fp.write('0\n')


def write_rigid_bodies(fp):
    # type: (typing.TextIO) -> None
    """
    Write "rigid bodies" section. So far, it is empty.
    :param fp: Output file descriptor
    """
    fp.write('0\n')


def write_tree(fp, tree):
    # type: (typing.TextIO, Optional[Tree]) -> None
    """
    Write "tree" section. Can be either free or anchored tree
    :param fp: Output file descriptor
    :param tree: Tree to print
    """
    if tree is None:
        fp.write('0\n')
    else:
        fp.write('1\n{}\n'.format(len(tree.nodes)))
        for node in tree.nodes:
            fp.write('{n} {data}\n'.format(n=len(node), data=' '.join(str(atom) for atom in node)))
        for edge in tree.edges:
            fp.write('{0} {1}\n'.format(*edge))


def main(f_out, f_pdbqt, f_pdb, anchored, shift):
    # type: (typing.TextIO, typing.TextIO, typing.TextIO, bool, int) -> None
    tree = build_tree(f_pdbqt.readlines(), f_pdb.readlines(), shift)
    write_free_atoms(f_out)
    write_rigid_bodies(f_out)
    if anchored:
        write_tree(f_out, None)  # free
        write_tree(f_out, tree)  # anchored
    else:
        write_tree(f_out, tree)  # free
        write_tree(f_out, None)  # anchored


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert PDQBT file to libsampling\'s tree structure')
    parser.add_argument('--pdbqt', '-q', required=True, type=argparse.FileType('r'),
                        help='Input PDBQT file. Must contain single molecule.')
    parser.add_argument('--pdb', '-p', required=True, type=argparse.FileType('r'),
                        help='Input PDB file. Must contain same molecule as PDBQT.')
    parser.add_argument('--output', '-o', nargs='?', type=argparse.FileType('w'), default=sys.stdout,
                        help='Output file. If not specified, write to stdout.')
    parser.add_argument('--shift', '-s', type=int, default=0,
                        help='Shift atom IDs in output by given number.')
    parser.add_argument('--anchored', '-a', action='store_true',
                        help='Make tree anchored (it will be free by default).')
    args = parser.parse_args()
    main(args.output, args.pdbqt, args.pdb, args.anchored, args.shift)
