#!/usr/bin/env python3

import io
import unittest

try:
    # Try loading Python2-only version first
    from StringIO import StringIO
except ImportError:
    # If it fails, load Python3-specific. It also exists in Python2, but requires meddling with Unicode
    from io import StringIO

try:
    # Different type of loading neighbor file depending on your environment...
    from . pdbqt_to_tree import *
except (ImportError, ValueError):
    from pdbqt_to_tree import *


REAL_PDBQT_DATA = """REMARK  9 active torsions:
REMARK  status: ('A' for Active; 'I' for Inactive)
REMARK    1  A    between atoms: C1_2  and  C2_5 
REMARK    2  A    between atoms: C5_8  and  C8_11 
REMARK    3  A    between atoms: N2_13  and  C15_24 
REMARK    4  A    between atoms: N3_18  and  S1_20 
REMARK    5  A    between atoms: C15_24  and  C16_25 
REMARK    6  A    between atoms: C16_25  and  O3_26 
REMARK    7  A    between atoms: C16_25  and  C17_27 
REMARK    8  A    between atoms: C17_27  and  N4_28 
REMARK    9  A    between atoms: C20_31  and  C23_34 
ROOT
ATOM      1  C8  X00     1      -5.294   9.539  -6.894  1.00  0.00     0.099 A 
ATOM      2  N1  X00     1      -4.078  10.068  -7.156  1.00  0.00    -0.170 NA
ATOM      3  N2  X00     1      -4.192  10.874  -8.202  1.00  0.00    -0.253 N 
ATOM      4  C9  X00     1      -5.468  10.863  -8.632  1.00  0.00     0.056 A 
ATOM      5  C10 X00     1      -6.194  10.010  -7.844  1.00  0.00     0.028 A 
ATOM      6  C11 X00     1      -5.994  11.442  -9.911  1.00  0.00     0.064 C 
ATOM      7  C12 X00     1      -7.520  11.222 -10.022  1.00  0.00     0.095 C 
ATOM      8  N3  X00     1      -7.960   9.883  -9.557  1.00  0.00    -0.175 NA
ATOM      9  C13 X00     1      -7.619   9.635  -8.141  1.00  0.00     0.117 C 
ENDROOT
BRANCH   3  10
ATOM     10  C15 X00     1      -3.058  11.560  -8.826  1.00  0.00     0.158 C 
BRANCH  10  11
ATOM     11  C16 X00     1      -3.267  12.773  -7.898  1.00  0.00     0.166 C 
BRANCH  11  12
ATOM     12  O3  X00     1      -4.247  13.629  -8.427  1.00  0.00    -0.381 OA
ATOM     13  H17 X00     1      -5.126  13.184  -8.288  1.00  0.00     0.211 HD
ENDBRANCH  11  12
BRANCH  11  14
ATOM     14  C17 X00     1      -1.955  13.566  -7.690  1.00  0.00     0.244 C 
BRANCH  14  15
ATOM     15  N4  X00     1      -2.048  14.521  -6.556  1.00  0.00    -0.023 N 
ATOM     16  H20 X00     1      -2.947  15.052  -6.654  1.00  0.00     0.283 HD
ATOM     17  C18 X00     1      -0.945  15.507  -6.638  1.00  0.00     0.214 C 
ATOM     18  C19 X00     1      -1.088  16.616  -5.575  1.00  0.00     0.037 C 
ATOM     19  C20 X00     1      -1.738  16.103  -4.268  1.00  0.00     0.028 C 
ATOM     20  C21 X00     1      -1.464  14.610  -4.080  1.00  0.00     0.037 C 
ATOM     21  C22 X00     1      -2.057  13.797  -5.247  1.00  0.00     0.214 C 
BRANCH  19  22
ATOM     22  C23 X00     1      -1.267  16.885  -3.064  1.00  0.00    -0.003 A 
ATOM     23  C26 X00     1      -2.047  17.518  -2.114  1.00  0.00     0.033 A 
ATOM     24  C29 X00     1      -3.431  17.645  -1.967  1.00  0.00     0.109 A 
ATOM     25  C25 X00     1      -1.199  18.029  -1.171  1.00  0.00     0.059 A 
ATOM     26  N6  X00     1      -3.912  18.292  -0.861  1.00  0.00    -0.182 N 
ATOM     27  O4  X00     1      -5.269  18.416  -0.693  1.00  0.00    -0.080 OA
ATOM     28  C28 X00     1      -3.068  18.819   0.087  1.00  0.00     0.110 A 
ATOM     29  C27 X00     1      -1.674  18.698  -0.046  1.00  0.00     0.038 A 
ATOM     30  N5  X00     1       0.081  17.756  -1.478  1.00  0.00    -0.352 N 
ATOM     31  H31 X00     1       0.930  18.011  -0.927  1.00  0.00     0.166 HD
ATOM     32  C24 X00     1       0.050  17.045  -2.629  1.00  0.00     0.068 A 
ENDBRANCH  19  22
ENDBRANCH  14  15
ENDBRANCH  11  14
ENDBRANCH  10  11
ENDBRANCH   3  10
BRANCH   1  33
ATOM     33  C5  X00     1      -5.532   8.542  -5.824  1.00  0.00     0.019 A 
ATOM     34  C4  X00     1      -4.462   7.759  -5.352  1.00  0.00     0.002 A 
ATOM     35  C3  X00     1      -4.668   6.801  -4.355  1.00  0.00     0.004 A 
ATOM     36  C2  X00     1      -5.946   6.605  -3.807  1.00  0.00     0.071 A 
ATOM     37  C7  X00     1      -7.011   7.404  -4.259  1.00  0.00     0.004 A 
ATOM     38  C6  X00     1      -6.801   8.371  -5.246  1.00  0.00     0.002 A 
BRANCH  36  39
ATOM     39  C1  X00     1      -6.138   5.552  -2.747  1.00  0.00     0.419 C 
ATOM     40  F2  X00     1      -7.451   5.495  -2.316  1.00  0.00    -0.166 F 
ATOM     41  F3  X00     1      -5.788   4.318  -3.261  1.00  0.00    -0.166 F 
ATOM     42  F1  X00     1      -5.327   5.837  -1.664  1.00  0.00    -0.166 F 
ENDBRANCH  36  39
ENDBRANCH   1  33
BRANCH   8  43
ATOM     43  S1  X00     1      -7.397   8.583 -10.586  1.00  0.00     0.228 S 
ATOM     44  C14 X00     1      -7.773   8.922 -12.295  1.00  0.00     0.157 C 
ATOM     45  O2  X00     1      -8.102   7.314 -10.197  1.00  0.00    -0.210 OA
ATOM     46  O1  X00     1      -5.912   8.385 -10.444  1.00  0.00    -0.210 OA
ENDBRANCH   8  43
TORSDOF 9
"""

REAL_PDB_DATA = """ATOM      1  F1  X00     1      -5.327   5.837  -1.664  1.00  0.00      X    F
ATOM      2  C1  X00     1      -6.138   5.552  -2.747  1.00  0.00      X    C
ATOM      3  F2  X00     1      -7.451   5.495  -2.316  1.00  0.00      X    F
ATOM      4  F3  X00     1      -5.788   4.318  -3.261  1.00  0.00      X    F
ATOM      5  C2  X00     1      -5.946   6.605  -3.807  1.00  0.00      X    C
ATOM      6  C3  X00     1      -4.668   6.801  -4.355  1.00  0.00      X    C
ATOM      7  C4  X00     1      -4.462   7.759  -5.352  1.00  0.00      X    C
ATOM      8  C5  X00     1      -5.532   8.542  -5.824  1.00  0.00      X    C
ATOM      9  C6  X00     1      -6.801   8.371  -5.246  1.00  0.00      X    C
ATOM     10  C7  X00     1      -7.011   7.404  -4.259  1.00  0.00      X    C
ATOM     11  C8  X00     1      -5.294   9.539  -6.894  1.00  0.00      X    C
ATOM     12  N1  X00     1      -4.078  10.068  -7.156  1.00  0.00      X    N
ATOM     13  N2  X00     1      -4.192  10.874  -8.202  1.00  0.00      X    N
ATOM     14  C9  X00     1      -5.468  10.863  -8.632  1.00  0.00      X    C
ATOM     15  C10 X00     1      -6.194  10.010  -7.844  1.00  0.00      X    C
ATOM     16  C11 X00     1      -5.994  11.442  -9.911  1.00  0.00      X    C
ATOM     17  C12 X00     1      -7.520  11.222 -10.022  1.00  0.00      X    C
ATOM     18  N3  X00     1      -7.960   9.883  -9.557  1.00  0.00      X    N
ATOM     19  C13 X00     1      -7.619   9.635  -8.141  1.00  0.00      X    C
ATOM     20  S1  X00     1      -7.397   8.583 -10.586  1.00  0.00      X    S
ATOM     21  O1  X00     1      -5.912   8.385 -10.444  1.00  0.00      X    O
ATOM     22  O2  X00     1      -8.102   7.314 -10.197  1.00  0.00      X    O
ATOM     23  C14 X00     1      -7.773   8.922 -12.295  1.00  0.00      X    C
ATOM     24  C15 X00     1      -3.058  11.560  -8.826  1.00  0.00      X    C
ATOM     25  C16 X00     1      -3.267  12.773  -7.898  1.00  0.00      X    C
ATOM     26  O3  X00     1      -4.247  13.629  -8.427  1.00  0.00      X    O
ATOM     27  C17 X00     1      -1.955  13.566  -7.690  1.00  0.00      X    C
ATOM     28  N4  X00     1      -2.048  14.521  -6.556  1.00  0.00      X    N
ATOM     29  C18 X00     1      -0.945  15.507  -6.638  1.00  0.00      X    C
ATOM     30  C19 X00     1      -1.088  16.616  -5.575  1.00  0.00      X    C
ATOM     31  C20 X00     1      -1.738  16.103  -4.268  1.00  0.00      X    C
ATOM     32  C21 X00     1      -1.464  14.610  -4.080  1.00  0.00      X    C
ATOM     33  C22 X00     1      -2.057  13.797  -5.247  1.00  0.00      X    C
ATOM     34  C23 X00     1      -1.267  16.885  -3.064  1.00  0.00      X    C
ATOM     35  C24 X00     1       0.050  17.045  -2.629  1.00  0.00      X    C
ATOM     36  N5  X00     1       0.081  17.756  -1.478  1.00  0.00      X    N
ATOM     37  C25 X00     1      -1.199  18.029  -1.171  1.00  0.00      X    C
ATOM     38  C26 X00     1      -2.047  17.518  -2.114  1.00  0.00      X    C
ATOM     39  C27 X00     1      -1.674  18.698  -0.046  1.00  0.00      X    C
ATOM     40  C28 X00     1      -3.068  18.819   0.087  1.00  0.00      X    C
ATOM     41  N6  X00     1      -3.912  18.292  -0.861  1.00  0.00      X    N
ATOM     42  O4  X00     1      -5.269  18.416  -0.693  1.00  0.00      X    O
ATOM     43  C29 X00     1      -3.431  17.645  -1.967  1.00  0.00      X    C
ATOM     44  H17 X00     1      -5.126  13.184  -8.288  1.00  0.00      X    H
ATOM     45  H20 X00     1      -2.947  15.052  -6.654  1.00  0.00      X    H
ATOM     46  H31 X00     1       0.930  18.011  -0.927  1.00  0.00      X    H
END
"""

RIGID_CLUSTER_REF = """0
0
1
10
9 2094 2095 2096 2097 2098 2099 2100 2101 2102
1 2107 
1 2108
2 2109 2127 
1 2110 
7 2111 2112 2113 2114 2115 2116 2128
11 2117 2118 2119 2120 2121 2122 2123 2124 2125 2126 2129
6 2088 2089 2090 2091 2092 2093 
4 2084 2085 2086 2087 
4 2103 2104 2105 2106
2108  2109
2114  2117
2110  2111
2108  2110
2107  2108
2096  2107
2088  2085
2094  2091
2101  2103
0
"""


class TestScript(unittest.TestCase):
    def test_map_pdbqt_to_pdb(self):
        pdb1_data = [
            'ATOM      1  C8  X00     1      -5.294   9.539  -6.894  1.00  0.00     0.099 A \n',
            'ATOM      2  C15 X00     1      -3.058  11.560  -8.826  1.00  0.00     0.158 C \n',
            'ATOM      3  C29 X00     1      -3.431  17.645  -1.967  1.00  0.00     0.109 A \n'
        ]
        pdb2_data = [
            'ATOM      1  C15 X00     1      -3.058  11.560  -8.826  1.00  0.00      X    C\n',
            'ATOM      2  C8  X00     1      -5.294   9.539  -6.894  1.00  0.00      X    C\n',
            'ATOM      3  C29 X00     1      -3.431  17.645  -1.967  1.00  0.00      X    C\n'
        ]
        pdb3_data = [
            'ATOM      1  C15 X00     1      -3.058  11.560  -8.826  1.00  0.00      X    C\n',
            'ATOM      2  C29 X00     1      -3.431  17.645  -1.967  1.00  0.00      X    C\n',
            'ATOM      3  C8  X00     1      -5.294   9.539  -6.894  1.00  0.00      X    C\n'
        ]
        map12 = map_pdbqt_to_pdb(pdb1_data, pdb2_data)
        self.assertDictEqual(map12, {1: 2, 2: 1, 3: 3})
        map21 = map_pdbqt_to_pdb(pdb2_data, pdb1_data)
        self.assertDictEqual(map21, {1: 2, 2: 1, 3: 3})
        map13 = map_pdbqt_to_pdb(pdb1_data, pdb3_data)
        self.assertDictEqual(map13, {1: 3, 2: 1, 3: 2})

    def test_tree_from_pdbqt(self):
        pdbqt_data = REAL_PDBQT_DATA.split('\n')
        tree = tree_from_pdbqt(pdbqt_data)
        self.assertListEqual(tree.nodes,
                             [
                                 list(range(1, 10)),
                                 [10],
                                 [11],
                                 [12, 13],
                                 [14],
                                 list(range(15, 22)),
                                 list(range(22, 33)),
                                 list(range(33, 39)),
                                 [39, 40, 41, 42],
                                 [43, 44, 45, 46]
                             ])
        self.assertListEqual(tree.edges,
                             [
                                 (11, 12),
                                 (19, 22),
                                 (14, 15),
                                 (11, 14),
                                 (10, 11),
                                 (3, 10),
                                 (36, 39),
                                 (1, 33),
                                 (8, 43)
                             ])

    def test_build_tree(self):
        pdbqt_data = REAL_PDBQT_DATA.split('\n')
        pdb_data = REAL_PDB_DATA.split('\n')
        tree = build_tree(pdbqt_data, pdb_data, 100)
        self.assertListEqual(tree.nodes,
                             [
                                 list(range(111, 120)),
                                 [124],
                                 [125],
                                 [126, 144],
                                 [127],
                                 [128, 129, 130, 131, 132, 133, 145],
                                 [134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 146],
                                 list(range(105, 111)),
                                 [101, 102, 103, 104],
                                 [120, 121, 122, 123]
                             ])
        self.assertListEqual(tree.edges,
                             [
                                 (125, 126),
                                 (131, 134),
                                 (127, 128),
                                 (125, 127),
                                 (124, 125),
                                 (113, 124),
                                 (105, 102),
                                 (111, 108),
                                 (118, 120)
                             ])

    def test_write_free_atoms(self):
        fo = StringIO()
        write_free_atoms(fo)
        self.assertEqual(fo.getvalue(), '0\n')

    def test_write_rigid_bodies(self):
        fo = StringIO()
        write_rigid_bodies(fo)
        self.assertEqual(fo.getvalue(), '0\n')

    def test_write_tree_empty(self):
        tree = Tree(nodes=[], edges=[])
        fo = StringIO()
        write_tree(fo, tree)
        self.assertEqual(fo.getvalue(), '1\n0\n')

    def test_write_tree_simple(self):
        tree = Tree(nodes=[[1, 2, 3], [4, 5]], edges=[(2, 5)])
        fo = StringIO()
        write_tree(fo, tree)
        self.assertEqual(fo.getvalue(), '1\n2\n3 1 2 3\n2 4 5\n2 5\n')

    def test_main(self):
        f_pdbqt = StringIO(REAL_PDBQT_DATA)
        f_pdb = StringIO(REAL_PDB_DATA)
        fo = StringIO()
        main(fo, f_pdbqt, f_pdb, anchored=False, shift=2083)
        # We split to ignore whitespace differences
        tokens = fo.getvalue().split()
        tokens_ref = RIGID_CLUSTER_REF.split()
        self.assertListEqual(tokens, tokens_ref)


if __name__ == '__main__':
    unittest.main()
