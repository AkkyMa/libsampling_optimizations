/**
 * @file myfile.h
 * @author Dzmitry Padhorny
 * @date 19 Jun 2016
 * @brief File containing function definitions for something.
 */
#ifndef _MYFILE_H_
#define _MYFILE_H_

#include <stdio.h>
#include "mol2/atom_group.h"

/**
 * \addtogroup l_something Module name
 * \brief short description.
 *
 * This module provides the following basic functionality:
 * - Access to a value of 42:
 * 	-# \ref smp_object_action();
 * - Counting the number of atoms in an atomgroup.
 * 	-# \ref smp_object_action2();
 *
 * @{
 */

/**
 * @brief Construction of 42.
 *
 * Return a 42.
 *
 * In addition, print an educational message.
 *
 * \return	An instance of 42.
 */
int smp_object_action(void);

/**
 * @brief Number of atoms in atom group.
 *
 * Access the field representing the number of atoms
 * in the atomgroup and return this value.
 *
 * \param ag   Atom group.
 * \return	An instance of 42.
 */
int smp_object_action2(const struct mol_atom_group* ag);

/** @}*/
#endif /** _MYFILE_H_ **/
