#ifndef _SMP_MC_H_
#define _SMP_MC_H_

#include <stdbool.h>
#include "prng.h"

bool smp_mc_metropolis(
	struct smp_prng *prng,
	const double e_old,
	const double e_new,
	const double kt);

#endif // _SMP_MC_H_
