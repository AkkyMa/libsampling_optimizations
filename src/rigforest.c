#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mol2/transform.h>
#include "rigforest.h"
#include "transform_exp.h"
#include "transform.h"
#include "utils.h"

//! This constant is used when we want to mark invalid value in index arrays (of type size_t)
#define INDEX_NOT_FOUND (SIZE_MAX)

/**
 * Read structure of protein from file to rigforest.
 *
 * \param rf Preallocated smp_rigforest structure to be filled.
 * \param ag Atomgroup. List of \c fixed atoms is changed.
 * \param filename Rigforest file name.
 * \param allocate_flag Should we allocate internal members of \p rf, or just fill them?
 */
static void _read_rigforest(
	struct smp_rigforest *rf,
	struct mol_atom_group *ag,
	const char *filename,
	const bool allocate_flag);

/**
 * Read rigtree definition from file \p fs to structure \p tr, and allocates internal structures if needed.
 *
 * \param tr Rigtree structure to be filled.
 * \param ag Atomgroup.
 * \param fs File handle from which rigtree structure is read.
 * \param start_atm Starting atom number in \p ag for our rigtree.
 * \param allocate_flag Should we allocate internal structures, or just fill them with updated info?
 */
static void _read_rigtree(
	struct smp_rigtree *tr,
	const struct mol_atom_group *ag,
	FILE *fs,
	const size_t start_atm,
	const bool allocate_flag);

/**
 * Uninitialize structure and free all memory pointers stored inside it. It does not free \p tr itself.
 */
static void _destroy_rigtree(struct smp_rigtree *tr);

/**
 * Uninitialize rigtree, free all allocated memory, including \p tr itself.
 */
static void _free_rigtree(struct smp_rigtree *tr);

/**
 * Displace all atoms in tree node by \c -cent, and then rotate them by \p R.
 * Or (which is the same), move coordinate origin to \p cent, and then rotate atoms by \p R.
 *
 * \param ag Atomgroup for new coordinates.
 * \param tr Rigtree.
 * \param node_index Node index.
 * \param cent Center of rotation.
 * \param rot Rotation matrix.
 * \param orig Array of coordinates of atoms.
 */
static void _move_and_rotate_node(
	struct mol_atom_group *ag,
	const struct smp_rigtree *tr,
	const size_t node_index,
	const struct mol_vector3 *cent,
	const struct mol_matrix3 *rot,
	const struct mol_vector3 *orig);

/**
 * Update \c tr->cent_t with new coordinates from atom_group.
 * \param tr Rigtree to be updated.
 * \param ag Atom group with new coordinates.
 */
static void _rigtree_update_center(struct smp_rigtree *tr, const struct mol_atom_group *ag);

/**
 * For every atom belonging to the tree, copy its coordinates from \p orig to \p ag.
 *
 * \param ag Atomgroup.
 * \param tr Rigtree.
 * \param orig Flattened array containing coordinates of tree atoms.
 */
static void _copy_coords_from_array_to_atomgroup(
	struct mol_atom_group *ag,
	const struct smp_rigtree *tr,
	const struct mol_vector3 *orig);

/**
 * Run BFS on tree, and update \c tr->parent, tr->bfs_qu, and tr->e_index.
 */
static void _bfs(struct smp_rigtree *tr);

/**
 * Change atom coordinates in \p ag so they correspond to the torsional state of tree \p tr.
 * This function only performs rotation of internal DoF.
 * To rotate rigtree as a whole, see \ref _move_rigidbody.
 * NOTE: It only changes coords in \p ag, not in \p orig or \p tr->orig.
 * NOTE: Initial coordinates in \p ag do not matter at all.
 *
 * \param ag Atomgroup.
 * \param tr Rigtree with desired rotation angles \c theta[i].
 * \param orig Atom coordinates corresponding to zero rotation (all \c theta[i]==0).
 */
static void _move_torsions(struct mol_atom_group *ag, const struct smp_rigtree *tr, const struct mol_vector3 *orig);

/**
 * Helper function for \ref _move_rigtree, that only rotates node \p index.
 */
static void _move_torsions_for_node(
	struct mol_atom_group *ag,
	const struct smp_rigtree *tr,
	const size_t node_index,
	const struct mol_vector3 *orig);

/**
 * Update coordinates in \p ag so they correspond to rotated and translated \p tr.
 * This function rotates and translates tree as a rigidbody, and does not concern itself with internal DoF.
 * See \ref _move_rigtree if you want to update coordinates according to internal rotations.
 */
static void _move_rigidbody(struct mol_atom_group *ag, const struct smp_rigtree *tr);

/**
 * Calculate the gradients w.r.t. 3 prarmeters for rotation and 3 parameters for translation.
 *
 * \param ag Atomgroup. Atom coordinates are updated. Must have gradients for each atom computed.
 * \param tr Rigtree.
 * \param orig Original (with zero rotation) coordinates of tree atoms.
 */
static void _derive_rigidbody(struct mol_atom_group *ag, struct smp_rigtree *tr, const struct mol_vector3 *orig);

/**
 * Calculate the gradients w.r.t. to the angles of rotatable bonds in tree.
 *
 * \param ag Atomgroup. Atom coordinates are updated. Must have gradients for each atom computed.
 * \param tr Rigtree.
 * \param orig Original atom coordinates.
 */
static void _derive_torsions(struct mol_atom_group *ag, const struct smp_rigtree *tr, const struct mol_vector3 *orig);

/**
 * Return the derivative of the energy function w.r.t. rotatable bond \p index.
 *
 * \param ag Atomgroup. Atom coordinates are updated. Must have gradients for each atom computed.
 * \param tr Rigtree.
 * \param index Index of bond.
 * \param orig Original atom coordinates.
 * \return Derivative of energy with respect to rotation of bond \p index.
 */
static double _derive_edge(
	struct mol_atom_group *ag,
	const struct smp_rigtree *tr,
	const size_t index,
	const struct mol_vector3 *orig);


/**
 * Update coordinates of atom in node \p index after rotating. Helper function for \ref _move_node.
 */
static void _rotate_node(
	struct mol_atom_group *ag,
	const struct smp_rigtree *tr,
	const size_t node_index,
	const struct mol_vector3 *cent,
	const struct mol_quaternion *q_R,
	const bool is_first,
	const struct mol_vector3 *orig);


struct smp_rigforest *smp_rigforest_create(
		const char *filename,
		struct mol_atom_group *ag)
{
	struct smp_rigforest *rf = calloc(1, sizeof(struct smp_rigforest));
	if (rf != NULL) {
		smp_rigforest_init(rf, filename, ag);
	}
	return rf;
}


void smp_rigforest_init(
		struct smp_rigforest *rf,
		const char *filename,
		struct mol_atom_group *ag)
{
	_read_rigforest(rf, ag, filename, 1);
}


void smp_rigforest_reload(
		struct smp_rigforest *rf,
		const char *filename,
		struct mol_atom_group *ag)
{
	_read_rigforest(rf, ag, filename, 0);
}


void smp_rigforest_destroy(struct smp_rigforest *rf)
{
	if (rf != NULL) {
		for (size_t i = 0; i < rf->nfree_trees; i++) {
			_free_rigtree(rf->free_trees[i]);
		}
		for (size_t i = 0; i < rf->nanch_trees; i++) {
			_free_rigtree(rf->anch_trees[i]);
		}
		free(rf->free_atoms);
		free(rf->free_atoms_coordinates);
		free(rf->free_atoms_gradients);
		free(rf->free_trees);
		free(rf->anch_trees);
		free(rf->original_coordinates);
	}
}


void smp_rigforest_free(struct smp_rigforest *rf)
{
	if (rf != NULL) {
		smp_rigforest_destroy(rf);
		free(rf);
	}
}


size_t smp_rigforest_ndim(const struct smp_rigforest *rf)
{
	size_t ndim = 3 * (rf->nfree_atoms);

	for (size_t i = 0; i < rf->nfree_trees; i++) {
		// nnodes-1 rotatable bonds, and 6 DoF of rigid body
		ndim += rf->free_trees[i]->nnodes + 5;
	}

	for (size_t i = 0; i < rf->nanch_trees; i++) {
		// nnodes-1 rotatable bonds
		ndim += rf->anch_trees[i]->nnodes - 1;
	}

	return ndim;
}


void smp_rigforest_copy_coords_to_atomgroup(struct mol_atom_group *ag, const struct smp_rigforest *rf)
{
	for (size_t i = 0; i < ag->natoms; i++) {
		MOL_VEC_COPY(ag->coords[i], rf->original_coordinates[i]);
	}
}


void smp_rigforest_copy_coords_from_atomgroup(struct smp_rigforest *rf, const struct mol_atom_group *ag)
{
	for (size_t i = 0; i < ag->natoms; i++) {
		MOL_VEC_COPY(rf->original_coordinates[i], ag->coords[i]);
	}
}


void smp_rigforest_update_from_coordinate_array(struct smp_rigforest *rf, struct mol_atom_group *ag, const double *array)
{
	smp_rigforest_copy_coords_to_atomgroup(ag, rf);

	for (size_t i = 0; i < rf->nfree_atoms; i++) {
		SMP_VEC_FROM_ARRAY(ag->coords[rf->free_atoms[i]], array, i);
	}

	size_t count = 3 * rf->nfree_atoms;

	for (size_t i = 0; i < rf->nfree_trees; i++) {
		for (size_t j = 0; j < rf->free_trees[i]->nnodes - 1; j++) {
			rf->free_trees[i]->edge_thetas[j] = array[count];
			count++;
		}

		struct mol_vector3 v;
		v.X = array[count];
		count++;
		v.Y = array[count];
		count++;
		v.Z = array[count];
		count++;
		rf->free_trees[i]->tree_rotation = smp_quaternion_from_exp(v);

		rf->free_trees[i]->tree_translation.X = array[count];
		count++;
		rf->free_trees[i]->tree_translation.Y = array[count];
		count++;
		rf->free_trees[i]->tree_translation.Z = array[count];
		count++;
	}

	for (size_t i = 0; i < rf->nanch_trees; i++) {
		for (size_t j = 0; j < rf->anch_trees[i]->nnodes - 1; j++) {
			rf->anch_trees[i]->edge_thetas[j] = array[count];
			count++;
		}
	}

	smp_rigforest_move(ag, rf);
}


void smp_rigforest_update_center(struct smp_rigforest *rf, const struct mol_atom_group *ag)
{
	for (size_t i = 0; i < rf->nfree_trees; i++) {
		_rigtree_update_center(rf->free_trees[i], ag);
	}
}


void smp_rigforest_move(struct mol_atom_group *ag, const struct smp_rigforest *rf)
{
	for (size_t i = 0; i < rf->nfree_trees; i++) {
		_move_torsions(ag, rf->free_trees[i], rf->original_coordinates);
		_move_rigidbody(ag, rf->free_trees[i]);
	}
	for (size_t i = 0; i < rf->nanch_trees; i++)
		_move_torsions(ag, rf->anch_trees[i], rf->original_coordinates);
}


void smp_rigforest_derive(double *restrict gradient, const struct smp_rigforest *rf, struct mol_atom_group *ag)
{
	if (gradient != NULL) {
		for (size_t i = 0; i < rf->nfree_atoms; i++) {
			SMP_VEC_TO_ARRAY(gradient, i, ag->gradients[rf->free_atoms[i]]);
		}

		size_t count = 3 * rf->nfree_atoms;

		for (size_t i = 0; i < rf->nfree_trees; i++) {
			_derive_torsions(ag, rf->free_trees[i], rf->original_coordinates);
			_derive_rigidbody(ag, rf->free_trees[i], rf->original_coordinates);

			for (size_t j = 0; j < rf->free_trees[i]->nnodes - 1; j++) {
				gradient[count] = rf->free_trees[i]->edge_grads[j];
				count++;
			}
			gradient[count++] = rf->free_trees[i]->tree_rotation_grads.X;
			gradient[count++] = rf->free_trees[i]->tree_rotation_grads.Y;
			gradient[count++] = rf->free_trees[i]->tree_rotation_grads.Z;
			gradient[count++] = rf->free_trees[i]->tree_translation_grads.X;
			gradient[count++] = rf->free_trees[i]->tree_translation_grads.Y;
			gradient[count++] = rf->free_trees[i]->tree_translation_grads.Z;
		}

		for (size_t i = 0; i < rf->nanch_trees; i++) {
			_derive_torsions(ag, rf->anch_trees[i], rf->original_coordinates);

			for (size_t j = 0; j < rf->anch_trees[i]->nnodes - 1; j++) {
				gradient[count] = rf->anch_trees[i]->edge_grads[j];
				count++;
			}
		}
	}
}


void smp_rigforest_minimize(int maxIt, double tol, struct smp_rigforest *rf, void *prms, lbfgs_evaluate_t egfun)
{
	size_t ndim = smp_rigforest_ndim(rf);

	double *xyz = calloc(ndim + 1, sizeof(double));

	for (size_t i = 0; i < rf->nfree_atoms; i++) {
		SMP_VEC_TO_ARRAY(xyz, i, rf->original_coordinates[i]);
	}

	lbfgsfloatval_t f_min;

	lbfgs_parameter_t param = {
		5, tol, 0, 1e-5,
		maxIt, LBFGS_LINESEARCH_DEFAULT, 40,
		1e-20, 1e20, 1e-4, 0.9, 0.9, 1.0e-16,
		0.0, 0, -1,
	};
	lbfgs((int) ndim, xyz, &f_min, egfun, NULL, prms, &param);

	free(xyz);
}


void _copy_coords_from_array_to_atomgroup(struct mol_atom_group *ag, const struct smp_rigtree *tr, const struct mol_vector3 *orig)
{
	for (size_t i = 0; i < tr->natoms; i++) {
		size_t ind = tr->atoms[i];
		MOL_VEC_COPY(ag->coords[ind], orig[ind]);
	}
}


void _bfs(struct smp_rigtree *tr)
{
	bool *was_visited = calloc(tr->nnodes, sizeof(bool));
	size_t *bfs_queue = calloc(tr->nnodes, sizeof(size_t));

	// the root does not have parent
	if (tr->nnodes > 0) {
		tr->_edge_index[0] = INDEX_NOT_FOUND;
	}

	size_t ind = 0;
	size_t fin = 1;
	// We always assume that the node 0 is the root
	if (tr->nnodes > 0) {
		bfs_queue[ind] = 0;
		was_visited[0] = true;
	}

	if (tr->nnodes > 0) {
		tr->node_parents[0] = INDEX_NOT_FOUND;  // We always assume that node 0 is the root of the tree
	}

	while (ind != fin && tr->nnodes > 0) {
		for (size_t j = 0; j < tr->node_degrees[bfs_queue[ind]]; j++) {
			size_t uu = tr->adjacency_matrix[bfs_queue[ind]][j];

			if (!was_visited[uu]) {
				was_visited[uu] = true;
				bfs_queue[fin] = uu;
				tr->node_parents[bfs_queue[fin]] = bfs_queue[ind];

				const size_t u1 = bfs_queue[ind];
				const size_t u2 = bfs_queue[fin];

				size_t index = INDEX_NOT_FOUND;
				// go through all rotatable bonds and find the one which has the ends u1 and u2 ( from parent to the child )
				for (size_t i = 0; i < tr->nnodes - 1; i++) {
					if ((index == INDEX_NOT_FOUND) &&
							(u1 == tr->node_label_for_atom[tr->edges[i].begin]) &&
							(u2 == tr->node_label_for_atom[tr->edges[i].end])) {
						index = i;
					}
				}

				tr->_edge_index[bfs_queue[fin]] = index;
				fin++;
			}
		}
		ind++;
	}

	free(was_visited);
	free(bfs_queue);
}


void _move_and_rotate_node(
		struct mol_atom_group *ag,
		const struct smp_rigtree *tr,
		const size_t node_index,
		const struct mol_vector3 *cent,
		const struct mol_matrix3 *rot,
		const struct mol_vector3 *orig)
{
	_copy_coords_from_array_to_atomgroup(ag, tr, orig);

	for (size_t i = 0; i < tr->nodes[node_index].natoms; i++) {
		size_t num = tr->nodes[node_index].atoms[i];

		// we impose the movement on the original coordinates of the atoms
		struct mol_vector3 pos;
		MOL_VEC_SUB(pos, orig[num], *cent);
		mol_vector3_rotate(&pos, rot);
		MOL_VEC_COPY(ag->coords[num], pos);
		// We're staying in the internal coordinates of the center of rotation
	}
}

void _rotate_node(
		struct mol_atom_group *ag,
		const struct smp_rigtree *tr,
		const size_t node_index,
		const struct mol_vector3 *cent,
		const struct mol_quaternion *q_R,
		const bool is_first,
		const struct mol_vector3 *orig)
{
	struct mol_vector3 pos;

	for (size_t i = 0; i < tr->nodes[node_index].natoms; i++) {
		size_t num = tr->nodes[node_index].atoms[i];

		if (is_first) {
			MOL_VEC_SUB(pos, orig[num], *cent);
		} else {
			MOL_VEC_SUB(pos, ag->coords[num], *cent);
		}

		smp_vector3_rotate_by_quaternion(&pos, q_R);
		MOL_VEC_ADD(ag->coords[num], pos, *cent);
	}
}


void _move_torsions_for_node(
		struct mol_atom_group *ag,
		const struct smp_rigtree *tr,
		const size_t node_index,
		const struct mol_vector3 *orig)
{
	// Function works as follows:
	// - Find the path from the node to the root
	// - Build all the rotation matrices and rotate the atoms in the corresponding rigid cluster

	// The path from node index to the root
	size_t *path = calloc(tr->nnodes, sizeof(size_t));

	size_t ind = 0;
	size_t v = node_index;

	// while we don't hit the root which is 0
	while (v != 0) {
		path[ind] = v;
		ind++;
		v = tr->node_parents[v];
	}

	// ind is the number of nodes in the path exclude the root, the path array is from 1..ind-1
	// The rotation start from the root until we get to the desired node

	struct mol_vector3 w, cent;
	struct mol_quaternion q;

	// The path is going from the end until we get to the root
	for (size_t i = 0; i < ind; i++) {
		size_t w_index = tr->_edge_index[path[i]];  // w_index is the index of the rotatable bond

		size_t s = tr->edges[w_index].begin; // start atom for the rotatable bond, -1 is because the atomgrop is start form 0, atoms in PDB from 1
		size_t e = tr->edges[w_index].end; // end atom for the rotatable bond
		MOL_VEC_SUB(w, orig[e], orig[s]);
		MOL_VEC_COPY(cent, orig[e]);

		smp_vector3_normalize(&w);
		mol_quaternion_from_axis_angle(&q, &w, tr->edge_thetas[w_index]);

		// here we need to rotate the ag atoms
		bool is_first = (i == 0);
		_rotate_node(ag, tr, node_index, &cent, &q, is_first, orig);
	}
	free(path);
}

// TODO: Issue #11
// 09/30/14, artem's comment:
// The way how _move_torsions and _move_torsions_for_node works now seems confusing and inefficient.  There is a
// loop over all nodes (starting from the second one since first one doesn't have bond) in _move_torsions function.
// Then inside _move_torsions_for_node function there is another loop over all the nodes until the current one. It means that
// starting from the third node the code does the same work twice, ie. goes through all the nodes before the
// current one and rotates them. This inefficiency has to do with the fact that you always start from the original
// coordinates. I think this two functions were designed to be general and supposed to work when we change only
// one dihedral. However, in this code it seems we always make changes to all dihedrals. Therefore, we can call
// _move_torsions_for_node only on the last node and it should update all the nodes.
void _move_torsions(struct mol_atom_group *ag, const struct smp_rigtree *tr, const struct mol_vector3 *orig)
{
	// We don't move the root as there is no rotatable bond with the end point as a root, so i starts at 1
	for (size_t i = 1; i < tr->nnodes; i++) {
		// Move all the rigid clusters by rotating nodes in the tree on by one
		_move_torsions_for_node(ag, tr, i, orig);
	}
}


double _derive_edge(
		struct mol_atom_group *ag,
		const struct smp_rigtree *tr,
		const size_t index,
		const struct mol_vector3 *orig)
{
	// TODO: Simplify and split into subroutines, but only after refactoring rodrigues.h. Issue #9
	size_t v = tr->edges[index].end; // the end atom of the rotatable bond
	v = tr->node_label_for_atom[v]; // find the rigid cluster that this atom belogns to

	int *mark = calloc(tr->nnodes, sizeof(int));
	size_t *que = calloc(tr->nnodes, sizeof(size_t));
	size_t *par = calloc(tr->nnodes, sizeof(size_t));

	size_t ind = 0;
	size_t fin = 1;
	que[ind] = v;
	mark[v] = 1;

	// The atoms which are important for calculating the derivative for rotatable bond index are the one
	// which are in the subtree rotated at vertex, so we do BFS and gather all of them in queue

	while (ind != fin) {
		for (size_t j = 0; j < tr->node_degrees[que[ind]]; j++) {
			if (mark[tr->adjacency_matrix[que[ind]][j]] == 0) {
				mark[tr->adjacency_matrix[que[ind]][j]] = 1;
				que[fin] = tr->adjacency_matrix[que[ind]][j];
				par[que[fin]] = que[ind];
				fin++;
			}
		}
		ind++;
	}

	// for each node from the above bfs keep the path from that node to the root
	size_t *path = calloc(tr->nnodes, sizeof(size_t));
	// keep the index of the rotatable bonds corresponding to the path to the root
	size_t *p_edg = calloc(tr->nnodes, sizeof(size_t));

	// keep the derivative of X,Y,Z coordinate of the atoms wrt the rotatable bond
	double *atom_coordinates_old = calloc(3 * ag->natoms, sizeof(double));

	struct mol_matrix3 rot_total;
	smp_matrix3_eye(&rot_total);

	// build the rotation matrix which corresponds to the rigid body motion
	struct mol_matrix3 rig_rot;
	mol_matrix3_from_quaternion(&rig_rot, tr->tree_rotation);

	// save back up of the coordinate of the atoms
	for (size_t i = 0; i < ag->natoms; i++) {
		SMP_VEC_TO_ARRAY(atom_coordinates_old, i, ag->coords[i]);
	}

	// assign the atoms the original coordinates
	for (size_t i = 0; i < tr->nnodes; i++) {
		for (size_t j = 0; j < tr->nodes[i].natoms; j++) {
			size_t atmnum = tr->nodes[i].atoms[j];
			MOL_VEC_SUB(ag->coords[atmnum], orig[atmnum], tr->tree_center);
		}
	}

	double grad = 0;

	for (size_t i = 0; i < ind; i++) {
		// u is the cluster of atoms that we want to find the derivative for them in this loop
		size_t u = que[i];

		size_t n = 0;
		path[n] = u;
		p_edg[n] = tr->_edge_index[u];

		// find the path form node u to the root & keep the bond
		while (u != 0) {
			u = tr->node_parents[u];
			n++;
			path[n] = u;
			p_edg[n] = tr->_edge_index[u];
		}
		n--;

		// We calculate the first part of the derivative which is Rn...R0(x-O0)

		smp_matrix3_eye(&rot_total);

		for (size_t j = 0; j <= n; j++) {
			struct mol_vector3 w;
			struct mol_matrix3 rotate;

			size_t s = tr->edges[p_edg[j]].begin;
			size_t e = tr->edges[p_edg[j]].end;

			MOL_VEC_SUB(w, orig[e], orig[s]);
			double wnorm = sqrt(MOL_VEC_SQ_NORM(w));

			if (path[j] != v) {
				double rt = tr->edge_thetas[p_edg[j]];
				if (wnorm != 0) {
					rt = rt / wnorm;
					MOL_VEC_MULT_SCALAR(w, w, rt);
				}
				smp_rmatr_from_exp(&rotate, w);
			} else {
				// when we reach vertex, we need to get the derivative of the rotation
				smp_rmatr_derive_wrt_theta(&rotate, w, tr->edge_thetas[p_edg[j]]);
			}
			mol_matrix3_multiply(&rot_total, &rot_total, &rotate); // rot_total = rotate * rot_total
		}

		// We need to find O_0 which
		size_t end_atom = tr->edges[p_edg[0]].end; // The end atom atom of the rotatable bond
		// Rotate the cluster u by rot_total and center of rotation O_0 but do not add the center of rotation at the end
		_move_and_rotate_node(ag, tr, que[i], &orig[end_atom], &rot_total, orig);
		// Now the coordinates of the atoms has the first part of the derivative

		smp_matrix3_eye(&rot_total);
		bool have_reached_the_end_atom = false;

		// the vector for the second part of the derivative
		struct mol_vector3 sol = { 0 };

		// The second part of the derivative start from the end of the queue for minimizing the cost of the calculation
		// The last node in the queue is R_0 which we don't have it for the second part of the derivative

		for (size_t j = n; j >= 1; j--) {
			struct mol_vector3 w;
			struct mol_matrix3 rotate;

			size_t s = tr->edges[p_edg[j]].begin;
			size_t e = tr->edges[p_edg[j]].end;

			MOL_VEC_SUB(w, orig[e], orig[s]);
			double wnorm = sqrt(MOL_VEC_SQ_NORM(w));

			if (path[j] != v) {
				double rt = tr->edge_thetas[p_edg[j]];
				if (wnorm != 0) {
					rt = rt / wnorm;
					MOL_VEC_MULT_SCALAR(w, w, rt);
				}
				smp_rmatr_from_exp(&rotate, w);
			}
			if (path[j] == v) {
				smp_rmatr_derive_wrt_theta(&rotate, w, tr->edge_thetas[p_edg[j]]);
				have_reached_the_end_atom = true;
			}

			// if we get to the vertex then the derivative is nonzero and
			// we should multiply it with the O_i-1-O_i from now
			mol_matrix3_multiply(&rot_total, &rotate, &rot_total); // rot_total = rot_total * rotate

			if (have_reached_the_end_atom) {
				size_t tmp1 = tr->edges[p_edg[j]].end; // O_i
				size_t tmp2 = tr->edges[p_edg[j - 1]].end; //it is the next in the queue but means it is the previous one in the path
				// as the p_edge build in this way O_i-1

				struct mol_vector3 r12;
				MOL_VEC_SUB(r12, orig[tmp2], orig[tmp1]);
				mol_vector3_rotate(&r12, &rot_total);
				MOL_VEC_ADD(sol, sol, r12);
			}
		}

		// add the first part and second part of the derivative
		// sol is the derivative for the second part
		for (size_t j = 0; j < tr->nodes[que[i]].natoms; j++) {
			const size_t ii = tr->nodes[que[i]].atoms[j];
			MOL_VEC_ADD(ag->coords[ii], ag->coords[ii], sol);
			mol_vector3_rotate(&ag->coords[ii], &rig_rot);
			grad -= MOL_VEC_DOT_PROD(ag->gradients[ii], ag->coords[ii]);
		}
	}

	for (size_t i = 0; i < ag->natoms; i++) {
		SMP_VEC_FROM_ARRAY(ag->coords[i], atom_coordinates_old, i);
	}

	free(mark);
	free(que);
	free(atom_coordinates_old);
	free(par);
	free(path);
	free(p_edg);
	return grad;
}


// Calculate the derivative wrt all parameters
void _derive_torsions(struct mol_atom_group *ag, const struct smp_rigtree *tr, const struct mol_vector3 *orig)
{
	// go through all the edges and for each of them find the derivative
	for (size_t i = 0; i < tr->nnodes - 1; i++)
		tr->edge_grads[i] = _derive_edge(ag, tr, i, orig);
}


void _read_rigforest(
		struct smp_rigforest *rf,
		struct mol_atom_group *ag,
		const char *filename,
		const bool allocate_flag)
{
	FILE *fp = fopen(filename, "r");

	fscanf(fp, "%zu", &(rf->nfree_atoms));

	if (ag->fixed == NULL) {
		ag->fixed = calloc(ag->natoms, sizeof(bool));
	}

	for (size_t i = 0; i < ag->natoms; i++) {
		ag->fixed[i] = 1;
	}

	if (allocate_flag) {
		rf->free_atoms = calloc(rf->nfree_atoms, sizeof(size_t));
		rf->free_atoms_coordinates = calloc(rf->nfree_atoms, sizeof(struct mol_vector3));
		rf->free_atoms_gradients = calloc(rf->nfree_atoms, sizeof(struct mol_vector3));
	}

	for (size_t i = 0; i < rf->nfree_atoms; i++) {
		fscanf(fp, "%zu", &(rf->free_atoms[i]));
		rf->free_atoms[i]--;
		MOL_VEC_COPY(rf->free_atoms_coordinates[i], ag->coords[rf->free_atoms[i]]);
		ag->fixed[rf->free_atoms[i]] = 0;
	}

	size_t numr;
	fscanf(fp, "%zu", &numr);
	if (numr > 0) {
		PRINTF_ERROR("Rigid bodies in forest not supported");
		exit(EXIT_FAILURE);
	}

	fscanf(fp, "%zu", &(rf->nfree_trees));
	if (allocate_flag) {
		rf->free_trees = calloc(rf->nfree_trees, sizeof(struct smp_rigtree *));
	}
	for (size_t i = 0; i < rf->nfree_trees; i++) {
		if (allocate_flag) {
			rf->free_trees[i] = calloc(1, sizeof(struct smp_rigtree));
		}
		_read_rigtree(rf->free_trees[i], ag, fp, 0, allocate_flag);
	}

	fscanf(fp, "%zu", &(rf->nanch_trees));
	if (allocate_flag) {
		rf->anch_trees = calloc(rf->nanch_trees, sizeof(struct smp_rigtree *));
	}
	for (size_t i = 0; i < rf->nanch_trees; i++) {
		if (allocate_flag) {
			rf->anch_trees[i] = calloc(1, sizeof(struct smp_rigtree));
		}
		_read_rigtree(rf->anch_trees[i], ag, fp, 0, allocate_flag);
	}

	if (allocate_flag) {
		rf->original_coordinates = calloc(ag->natoms, sizeof(struct mol_vector3));
	}

	smp_rigforest_copy_coords_from_atomgroup(rf, ag);
	fclose(fp);
}


void _read_rigtree(
		struct smp_rigtree *tr,
		const struct mol_atom_group *ag,
		FILE *fs,
		const size_t start_atm,
		const bool allocate_flag)
{
	fscanf(fs, "%zu", &(tr->nnodes));
	if (allocate_flag) {
		tr->nodes = calloc(tr->nnodes, sizeof(struct smp_rigtree_node));
		tr->node_label_for_atom = calloc(ag->natoms, sizeof(size_t));
		tr->edge_grads = calloc(tr->nnodes-1, sizeof(double));

		tr->node_degrees = calloc(tr->nnodes, sizeof(size_t));
		tr->edges = calloc(tr->nnodes, sizeof(struct smp_rigtree_edge));
	}

	tr->tree_rotation.W = 1;
	tr->tree_rotation.X = tr->tree_rotation.Y = tr->tree_rotation.Z = 0;
	tr->tree_translation.X = tr->tree_translation.Y = tr->tree_translation.Z = 0;

	// -1 are the atoms of the atomgroup which do not belong to the rigtree
	for (size_t i = 0; i < ag->natoms; i++)
		tr->node_label_for_atom[i] = INDEX_NOT_FOUND;

	tr->natoms = 0; // the number of atoms of the rigtree

	for (size_t i = 0; i < tr->nnodes; i++) {
		size_t num;
		fscanf(fs, "%zu", &num);
		tr->nodes[i].label = i;  // we label the nodes according to their order in the input file
		tr->nodes[i].natoms = num;
		if (allocate_flag) {
			tr->nodes[i].atoms = calloc(num, sizeof(size_t));
		}

		tr->natoms += tr->nodes[i].natoms;

		for (size_t j = 0; j < num; j++) {
			size_t atom_id, atom_id_raw;
			fscanf(fs, "%zu", &atom_id_raw);

			// PDB starting index for atoms is 1, but in libmol it is 0, we shift the index by 1
			atom_id = atom_id_raw + start_atm - 1;
			if (atom_id >= ag->natoms) {
				PRINTF_ERROR("Can not add atom with id %zu (%zu) to tree: atomgroup only has %zu atoms!",
					atom_id_raw, atom_id, ag->natoms);
				exit(EXIT_FAILURE);
			}
			tr->nodes[i].atoms[j] = atom_id;
			tr->node_label_for_atom[atom_id] = i;
			ag->fixed[atom_id] = false;
		}
	}

	if (allocate_flag) {
		tr->atoms = calloc(tr->natoms, sizeof(size_t));
	}

	int count = 0;
	for (size_t i = 0; i < ag->natoms; i++) {
		if (tr->node_label_for_atom[i] != INDEX_NOT_FOUND) {
			tr->atoms[count] = i;
			count++;
		}
	}

	// we consider the tree as a directed tree
	// this is our assumption that the input is a directed tree if we want to generalize the code
	// then we need to have a routine to get undirected tree and make it a directed one
	for (size_t i = 0; i < tr->nnodes - 1; i++) {
		size_t v1, v2;
		// FIXME: The order of vertex of the edges matters, but it should not. Issue #16
		fscanf(fs, "%zu %zu ", &v1, &v2);

		v1 = v1 + start_atm;
		v2 = v2 + start_atm;

		tr->edges[i].begin = v1 - 1;
		tr->edges[i].end = v2 - 1;

		// the nodes of the edges are the atoms but we need the adjacency matrix based on the nodes
		size_t c1 = tr->node_label_for_atom[v1 - 1];
		tr->node_degrees[c1]++;
	}

	_rigtree_update_center(tr, ag);

	if (allocate_flag) {
		tr->adjacency_matrix = calloc(tr->nnodes, sizeof(size_t *));
		for (size_t i = 0; i < tr->nnodes; i++) {
			tr->adjacency_matrix[i] = calloc(tr->node_degrees[i], sizeof(size_t));
		}
	}
	for (size_t i = 0; i < tr->nnodes; i++) {
		tr->node_degrees[i] = 0;
	}

	// fill the adjacency matrix for the tree based on nodes
	for (size_t i = 0; i < tr->nnodes - 1; i++) {
		size_t v1 = tr->edges[i].begin;
		size_t v2 = tr->edges[i].end;

		v1 = tr->node_label_for_atom[v1];
		v2 = tr->node_label_for_atom[v2];

		tr->adjacency_matrix[v1][tr->node_degrees[v1]] = v2;

		tr->node_degrees[v1]++;
	}

	// For each edge which correspond to a rotatable bond, theta is the amount of rotation
	if (allocate_flag) {
		tr->edge_thetas = calloc(tr->nnodes - 1, sizeof(double));
	}
	for (size_t i = 0; i < tr->nnodes - 1; i++) {
		tr->edge_thetas[i] = 0;
	}


	// for node in the BFS queue we keep the index of edge which connect this node to its parent
	tr->_edge_index = calloc(tr->nnodes, sizeof(size_t));
	// for each node we keep the parent in the tree
	tr->node_parents = calloc(tr->nnodes, sizeof(size_t));
	_bfs(tr);
}


void _rigtree_update_center(struct smp_rigtree *tr, const struct mol_atom_group *ag)
{
	struct mol_vector3 cent = { 0 };

	for (size_t j = 0; j < tr->natoms; j++) {
		size_t index = tr->atoms[j];
		MOL_VEC_ADD(cent, cent, ag->coords[index]);
	}

	MOL_VEC_DIV_SCALAR(tr->tree_center, cent, tr->natoms);
}


void _move_rigidbody(struct mol_atom_group *ag, const struct smp_rigtree *tr)
{
	struct mol_vector3 pos;

	for (size_t i = 0; i < tr->nnodes; i++) {
		for (size_t j = 0; j < tr->nodes[i].natoms; j++) {
			size_t tmp_index = tr->nodes[i].atoms[j];

			MOL_VEC_SUB(pos, ag->coords[tmp_index], tr->tree_center);
			smp_vector3_rotate_by_quaternion(&pos, &tr->tree_rotation);
			MOL_VEC_ADD(pos, pos, tr->tree_translation);
			MOL_VEC_ADD(pos, pos, tr->tree_center);
			MOL_VEC_COPY(ag->coords[tmp_index], pos);
		}
	}
}

/**
 * Compute dot product (\p grad, \c dvdR * \p pos).
 * \param result The resulting vector is a dot product (\p grad, \c dvdR * \p pos).
 * where \c dR_over_dv is derivative of rotation matrix w.r.t. v[i].
 */
static void _rmatr_convolution(
		struct mol_vector3 *result,
		const struct mol_matrix3 *dr_over_dv,
		const struct mol_vector3 *pos,
		const struct mol_vector3 *grad)
{
	struct mol_vector3 temp;
	temp = *pos;
	mol_vector3_rotate(&temp, &dr_over_dv[0]);
	result->X = MOL_VEC_DOT_PROD(temp, *grad);

	temp = *pos;
	mol_vector3_rotate(&temp, &dr_over_dv[1]);
	result->Y = MOL_VEC_DOT_PROD(temp, *grad);

	temp = *pos;
	mol_vector3_rotate(&temp, &dr_over_dv[2]);
	result->Z =  MOL_VEC_DOT_PROD(temp, *grad);
}



static void _derive_rigidbody(struct mol_atom_group *ag, struct smp_rigtree *tr, const struct mol_vector3 *orig)
{
	struct mol_vector3 *atom_coordinates_old = calloc(tr->natoms, sizeof(struct mol_vector3));

	for (size_t i = 0; i < tr->natoms; i++) {
		size_t ind = tr->atoms[i];
		atom_coordinates_old[i] = ag->coords[ind];
	}

	_copy_coords_from_array_to_atomgroup(ag, tr, orig);

	// Move the rotatable bonds
	_move_torsions(ag, tr, orig);

	// Calculate the derivative wrt the rotation and translation
	MOL_VEC_SET_SCALAR(tr->tree_rotation_grads, 0);
	MOL_VEC_SET_SCALAR(tr->tree_translation_grads, 0);

	struct mol_vector3 tree_rot;
	struct mol_matrix3 dv_over_dr[3];
	tree_rot = smp_quaternion_to_exp(tr->tree_rotation);
	smp_rmatr_derive_wrt_exp(dv_over_dr, tree_rot);

	for (size_t i = 0; i < tr->natoms; i++) {
		const size_t ind = tr->atoms[i];

		struct mol_vector3 result, pos, neg_grad;

		MOL_VEC_MULT_SCALAR(neg_grad, ag->gradients[ind], -1);
		MOL_VEC_SUB(pos, ag->coords[ind], tr->tree_center);

		_rmatr_convolution(&result, dv_over_dr, &pos, &neg_grad);

		MOL_VEC_ADD(tr->tree_rotation_grads, tr->tree_rotation_grads, result);
		MOL_VEC_ADD(tr->tree_translation_grads, tr->tree_translation_grads, neg_grad);
	}

	for (size_t i = 0; i < tr->natoms; i++) {
		const size_t ind = tr->atoms[i];
		ag->coords[ind] = atom_coordinates_old[i];
	}

	free(atom_coordinates_old);
}


void _destroy_rigtree(struct smp_rigtree *tr)
{
	if (tr != NULL) {
		for (size_t i = 0; i < tr->nnodes; i++) {
			free(tr->nodes[i].atoms);
			free(tr->adjacency_matrix[i]);
		}
		free(tr->nodes);
		free(tr->node_label_for_atom);
		free(tr->edge_grads);
		free(tr->node_degrees);
		free(tr->edges);
		free(tr->atoms);
		free(tr->edge_thetas);
		free(tr->_edge_index);
		free(tr->node_parents);
	}
}


void _free_rigtree(struct smp_rigtree *tr)
{
	if (tr != NULL) {
		_destroy_rigtree(tr);
		free(tr);
	}
}
