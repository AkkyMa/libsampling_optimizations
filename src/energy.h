#ifndef _SMP_ENERGY_H_
#define _SMP_ENERGY_H_

/**
 * \addtogroup l_energy Energy
 * \brief
 * Module contains functions for calculating energies that are not part of libmol2 but are
 * necessary for studied MC protocols.
 *
 * @{
*/

#include <mol2/atom_group.h>
#include <mol2/prms.h>
#include <mol2/nbenergy.h>

/**
 * Calculate Van der Waals potential with linear short-range term (linear ramp).
 * When atoms overlap significantly, the linear energy function is used to gently push them apart.
 * For larger distances, classical Van der Waals potential (similar to \ref vdweng from libmol) is used.
 * This potential corresponds to the LJ potential used in Rosetta's \c core::scoring::etable.
 *
 * Attraction and repulsion energies are calculated separately.
 *
 * The formula is:
 *
 * If \f$ d < C_{line} r_{ij}, \f$ then \f$ E = w_{rep} \epsilon_{ij} (a + b(d - C_{line} r_{ij}))\f$
 *
 * \f$ a = \frac{1}{C_{line}^{12}} -2 \frac{1}{C_{line}^6} + \frac{r_{ij}^6}{d_{cutoff}^6}\left(4 - 2 C_{line}^6 \frac{r_{ij}^6}{d_{cutoff}^6}\right) + \frac{r_{ij}^{12}}{d_{cutoff}^{12}} \left(2 C_{line}^6\frac{r_{ij}^6}{d_{cutoff}^6} -3\right) \f$
 *
 * \f$ b = \frac{12}{C_{line}r_{ij}}\left(-\frac{1}{C_{line}^{12}} + \frac{1}{C_{line}^{6}} + C_{line}^6 \frac{r_{ij}^6}{d_{cutoff}^6}\left(\frac{r_{ij}^{12}}{d_{cutoff}^{12}} - \frac{r_{ij}^6}{d_{cutoff}^6}\right)\right) \f$
 *
 * If \f$ d < C_{rep} r_{ij}, \f$ then \f$ E = w_{rep} \epsilon_{ij} r^{*}\f$
 *
 * If \f$ d < d_{cutoff}, \f$ then \f$ E = w_{atr} \epsilon_{ij} r^{*}\f$
 *
 * \f$ where \ r^{*} = \frac{r_{ij}^{12}}{d^{12}} - 2 \frac{r_{ij}^{6}}{d^{6}} + \frac{r_{ij}^{6}}{d_{cutoff}^{6}}\left(4 - 2\frac{d^{6}}{d_{cutoff}^{6}}\right) + \frac{r_{ij}^{12}}{d_{cutoff}^{12}}\left(2 \frac{d^6}{d_{cutoff}^6} - 3\right)  \f$
 *
 * \f$ r_{ij} = r^{vdw}_i + r^{vdw}_j \f$
 *
 * \f$ d_{cutoff} \f$ = \c nblst-> nbcon
 *
 * To work with 03-bonds use \ref smp_vdwengs03_rep_line.
 * \param ag Atomgroup; atom gradients are changed.
 * \param energy_rep The pointer where resulting repulsion energy is added. Just a single \c double, not an array.
 * \param energy_atr The pointer where resulting energy of attraction is added. Just a single \c double, not an array.
 * \param nblst Non-bonded list.
 * \param weight_rep Repulsion energy scale. Default recommended value is 0.05.
 * \param weight_atr Attraction energy scale. Default recommended value is 0.05.
 */
void smp_energy_vdw_rep_line(
	struct mol_atom_group *ag,
	double *energy_rep,
	double *energy_atr,
	const struct nblist *nblst,
	const double weight_rep,
	const double weight_atr);

/**
 * Calculate Van der Waals potential with linear short-range term, only for 03 pairs, using corresponding VdW parameters.
 * See \ref smp_vdweng_rep_line for more detailed description of potential.
 *
 * \param ag Atomgroup; atom gradients are changed.
 * \param energy_rep The pointer where resulting repulsion energy is added. Just a single \c double, not an array.
 * \param energy_atr The pointer where resulting energy of attraction is added. Just a single \c double, not an array.
 * \param nblst Non-bonded list.
 * \param n03 Number of 03-bonds.
 * \param list03 Array of length \c 2*n03. The i-th 03-bond is between atoms \c list03[2*i] and \c list03[2*i+1].
 * \param weight_rep Repulsion energy scale. Default recommended value is 0.05.
 * \param weight_atr Attraction energy scale. Default recommended value is 0.05.
 */
void smp_energy_vdw03_rep_line(
	struct mol_atom_group *ag,
	double *energy_rep,
	double *energy_atr,
	const struct nblist *nblst,
	const int n03,
	const int *list03,
	const double weight_rep,
	const double weight_atr);

/**
 * Calculate Van der Waals potential with linear short-range term, only between ligand and the rest of the system.
 * See \ref smp_vdweng_rep_line for more detailed description of potential.
 *
 * Ligand is defined by indexes of its first (\p ligand_atom_first) and last (\p ligand_atom_last) atoms (inclusive).
 * Only intractions between ligand and the rest of the atomgroup are computed.
 *
 * \param ag Atomgroup; atom gradients are changed.
 * \param energy_rep The pointer where resulting energy is added. Just a single \c double, not an array.
 * \param nblst Non-bonded list.
 * \param ligand_atom_first Index (in \p ag) of the first atom belonging to the ligand.
 * \param ligand_atom_last Index (in \p ag) of the last atom belonging to the ligand.
 * \param weight_rep Repulsion energy scale. Default recommended value is 1.0.
 * \param weight_atr Attraction energy scale. Default recommended value is 1.0.
 */
void smp_energy_vdw_rep_line_ligand(
	struct mol_atom_group *ag,
	double *energy,
	const struct nblist *nblst,
	const int ligand_atom_first,
	const int ligand_atom_last,
	const double weight_atr,
	const double weight_rep);

/**
 * Calculates Coulomb energies with a distant-dependent dielectric.
 * Attention: for distance < 3 A the energy is constant, but gradients are not zero! (see Issue #14)
 * Attention: does not work with 03-bonds.
 *
 * The formula is:
 *
 * If \f$d < 3 A\f$, then \f$ E = C \frac{q_i q_j}{9} w_{short} E_{shift} \f$
 *
 * If \f$ d < 5 A\f$, then \f$ E_{Coulomb} = C \frac{q_{1} q_{2} w_{short} E_{shift}}{d^2} \f$
 *
 * If \f$ d < d_{cutoff}\f$, then \f$ E_{Coulomb} =  C \frac{q_{1} q_{2} w_{long} E_{shift}}{d^2} \f$
 *
 * \f$ E_{shift} = \left(1 - \frac{d}{d_{cutoff}}\right)^2 \f$
 *
 * \f$ d_{cutoff} \f$ = \c nblst-> nbcon
 *
 * \param ag Atomgroup; atom gradients are changed.
 * \param energy_short The pointer where resulting short energy is added. Just a single \c double, not an array.
 * \param energy_long The pointer where resulting long energy is added. Just a single \c double, not an array.
 * \param nblst nblst Non-bonded list.
 * \param weight_short Short-range energy scale. Default recommended value is 0.025.
 * \param weight_long Long-range energy scale. Default recommended value is 0.025.
 */
void smp_energy_coul_rdie_shift(
	struct mol_atom_group *ag,
	double *energy_short,
	double *energy_long,
	const struct nblist *nblst,
	const double weight_short,
	const double weight_long);

/**
 * Calculate quadratic retstraint potential.
 *
 * \f$ e = w \cdot \sum_i \left|\vec{r_i} - \vec{r_i^0}\right|^2,\f$
 * where \f$w\f$ is \p weight, \f$r_i\f$ is current atom position, and \f$r_i^0\f$ is position from \p reference_coords.
 *
 * \param ag Atomgroup; atom gradients are changed.
 * \param energy The pointer where resulting short energy is added. Just a single \c double, not an array.
 * \param atom_list List of indices of atoms that are restrained.
 * \param reference_coords Original coordinates of restrained atoms. Length: \c atom_list.size.
 * \param weight Energy scale. Recommended value: 1.0.
 */
void smp_energy_restraints_fixedpoint(
	struct mol_atom_group *ag,
	double *energy,
	const struct mol_index_list atom_list,
	const struct mol_vector3 *reference_coords,
	const double weight);


void smp_energy_dars(
	struct mol_atom_group *ag,
	double *energy,
	const struct mol_prms *atomprm,
	const struct nblist *nblst,
	const double weight) __attribute__((deprecated));

/** @}*/
#endif /** _SMP_ENERGY_H_ **/
