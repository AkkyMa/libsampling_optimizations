#ifndef _SMP_PRNG_H_
#define _SMP_PRNG_H_

/**
 * \addtogroup l_prng PRNG
 * \brief Module to work with pseudo random number generator as \ref smp_prng structure
 *
 * @{
 */

#include <mol2/quaternion.h>

/**
 * Internal state vector length for MT19937 PRNG
 */
#define SMP_PRNG_MT_N 624

/**
 * Type of pseudorandom number generator
 */
enum smp_prng_type {
	PRNG_MT19937, /**< Mersenne twister implementation (MT19937) */
};

/**
 *  PRNG structure declaration
 */
struct smp_prng {
	unsigned long mt[SMP_PRNG_MT_N]; /**< the array for the state vector  */
	int mti; /**< mti==SMP_PRNG_MT_N+1 means mt[SMP_PRNG_MT_N] is not initialized */
};

/**
 * Initialises \ref smp_prng structure fields according to seed value.
 * Calls appropriate internal initialisation function for the chosen generator type.
 * \param smp_prng pseudo-random number generator
 * \param seed initial value
 * \param type type of chosen PRNG
 */
void smp_prng_init(struct smp_prng *smp_prng, const unsigned long seed, const enum smp_prng_type type);

/**
 * Allocate memory for \ref smp_prng structure and call \ref smp_prng_init if chosen \p type is valid.
 * \param seed initial value
 * \param type type of chosen PRNG
 * \return smp_prng pseudo-random number generator
 */
struct smp_prng *smp_prng_create(const unsigned long seed, const enum smp_prng_type type);

/**
 * Deallocates \ref smp_prng structure
 * \param smp_prng pseudo-random number generator
 */
void smp_prng_free(struct smp_prng *smp_prng);

/**
 * Destroys \ref smp_prng structure
 * \param smp_prng pseudo-random number generator
 */
void smp_prng_destroy(struct smp_prng *smp_prng);

/**
 * Generates a 32-bit random number.
 * \param smp_prng pseudo-random number generator
 * \return generated value
 */
unsigned long smp_prng_uint32(struct smp_prng *prng);

/**
 * Generates a double-precision random number distributed uniformly on (0,1)-real-interval
 * \param smp_prng pseudo-random number generator
 * \return generated value
 */
double smp_prng_uniform(struct smp_prng *smp_prng);

/**
 * Gnerates a single-precision random number distributed uniformly on (0,1)-real-interval
 * \param smp_prng pseudo-random number generator
 * \return generated value
 */
float smp_prng_uniformf(struct smp_prng *smp_prng);

/**
 * Generates a double-precision random number distributed normally
 * \param smp_prng pseudo-random number generator
 * \return generated value
 */
double smp_prng_gaussian(struct smp_prng *smp_prng);

/**
 * Generates pair of double-precision random numbers according to a bivariate normal distribution
 * \param smp_prng pseudo-random number generator
 * \param r1 where to store first generated value
 * \param r2 where to store second generated value
 */
void smp_prng_gaussian2(struct smp_prng *smp_prng, double *r1, double *r2);

/**
 * Generates a single-precision random number distributed normally
 * \param smp_prng pseudo-random number generator
 * \return generated value
 */
float smp_prng_gaussianf(struct smp_prng *smp_prng);

/**
 * Generates pair of single-precision random numbers according to a bivariate normal distribution
 * \param smp_prng pseudo-random number generator
 * \param r1 where to store first generated value
 * \param r2 where to store second generated value
 */
void smp_prng_gaussian2f(struct smp_prng *smp_prng, float *r1, float *r2);

/**
 * Generates a quaternion corresponding to a random rotation.
 * The values are uniformly-distributed according to Haar measure, that is, the most intuitive definition of "uniform" for SO(3).
 * The algorithm used is the Subgroup Algorithm from [1].
 *
 * Be prepared that the information on random rotations is scarce.
 *
 * 1. Shoemake, K. Uniform random rotations. in Graphics Gems III (IBM Version) 124–132 (Elsevier, 1992). doi:10.1016/B978-0-08-050755-2.50036-1
 *
 * \param smp_prng pseudo-random number generator
 * \param q where to store generated quaternion
 */
void smp_prng_quaternion(struct smp_prng *smp_prng, struct mol_quaternion *q);

/**
 * Generates a quaternion corresponding to a small random rotation.
 * Internally, uses \ref smp_prng_quaternion and Slerp (spherical linear interpolation) [2].
 * The produced quaternion rotates any vector by no more than \p delta * pi radians.
 *
 * 2. Shoemake, K. Animating rotation with quaternion curves. ACM SIGGRAPH Comput. Graph. 19, 245–254 (1985).
 *
 * \param smp_prng pseudo-random number generator
 * \param q where to store generated quaternion
 * \param delta max rotation angle (in units of pi). Should be in range [0;1].
 */
void smp_prng_quaternion_small(struct smp_prng *smp_prng, struct mol_quaternion *q, const double delta);

/** @}*/
#endif  /** _SMP_PRNG_H_ **/
