#ifndef _SMP_MOVERS_H_
#define _SMP_MOVERS_H_

#include <mol2/atom_group.h>
#include <mol2/nbenergy.h>
#include <mol2/gbsa.h>
#include "prng.h"
#include "rigforest.h"

/**
 * \addtogroup movers
 * \brief 
 *
 * @{
 */

/**
 * Given ligand and receptor in the same \p ag, pull them apart or push them together so their backbone-to-backbone VdW
 * energy (\ref smp_energy_vdw_rep_line_ligand) is within acceptable limits.
 *
 * If energy > \p energy_threshold_pull, then the backbones clash and we pull ligand away from receptor, until we
 * have energy <= \p energy_threshold_pull.
 *
 * If energy == 0, then the ligand and receptor are too fat apart, and we pushes ligand towards the receptor until we
 * have energy > \p energy_threshold_push.
 *
 * The sliding is performed along the line connecting geometric centers of ligand and receptor interfaces.
 *
 * \param ag Atomgroup.
 * \param ags General non-bonded list. It is updated during sliding process and is necessary for this function.
 * \param acs GBSA-specific non-bonded list (optional, can be \c NULL). Unlike \p ags is not necessary, and is only provided
 * for convenience so you don't have to update it manually. Set to \c NULL if you don't have it of don't care about it.
 * \param t[out] The resulting translation vector of a ligand.
 * \param lig_atom_list Indices of atoms if \p ag that belong to ligand. For now must be ordered and without gaps!
 * \param interface_residue_list Indices of residues (in \c ag->residue_list) that constitute the interface. Used only
 * for calculating the sliding direction. Must contain both receptor and ligand residues, in no particular order.
 * If residue contains atoms from \p lig_atom_list, it is part of ligand interface. Otherwise it's part of receptor interface.
 * \param step Sliding step, in Angstrom. We move ligand step-by-step until the energy is satisfactory.
 * \param energy_threshold_pull Lower bound of VdW energy signifying clash.
 * \param energy_threshold_push Upper bound of VdW energy signifying the absense of interaction.
 * \return Number of sliding steps performed.
 */
unsigned int smp_movers_slide_into_contact_backbone(
	struct mol_atom_group *ag,
	struct agsetup *ags,
	struct acesetup *acs,
	struct mol_vector3 *t,
	const struct mol_index_list lig_atom_list,
	const struct mol_index_list interface_residue_list,
	const double step,
	const double energy_threshold_pull,
	const double energy_threshold_push);


/**
 * Randomly rotate and translate group of atoms (given by \p atomlist) as a rigid body.
 * Rotation is performed around the centroid of \p atomlist.
 * Translation and rotation is the same for all atoms in \p atomlist, so it behaves as a rigid body.
 *
 * Translation vector components are i.i.d. uniform random variables in [- \p delta_translation/2; + \p delta_translation/2] range.
 * Rotation is given a random uniform quaternion (see \ref smp_prng_quaternion_small for details).
 *
 * \param ag Atomgroup.
 * \param prng PRNG.
 * \param atom_list List of atoms to move. All atoms in \p ag that are not in this list are ignored and have no effect.
 * \param delta_translation Maximum translation, in Angstroms. Should be > 0.
 * \param delta_rotation Maximum rotation, in radians.
 */
void smp_movers_randomly_rotate_translate_selected(
	struct mol_atom_group *ag,
	struct smp_prng *prng,
	const struct mol_index_list atom_list,
	const double delta_translation,
	const double delta_rotation);


/**
 * Randomly rotate dihedrals in Rigid Forest.
 *
 * Rotate all dihedrals in all anchored and free trees in \p rigforest by i.i.d. uniformly generated random values in [-delta/2; +delta/2] range.
 * Coordinates in \p ag are updated.
 *
 * \param ag Atomgroup.
 * \param rigforest Rigid forest.
 * \param prng PRNG.
 * \param delta Maximum rotation, in radians.
 */
void smp_movers_randomly_rotate_dihedrals(
	struct mol_atom_group *ag,
	struct smp_rigforest *rigforest,
	struct smp_prng *prng,
	const double delta);

/** @}*/
#endif /* _SMP_MOVERS_H_ */
