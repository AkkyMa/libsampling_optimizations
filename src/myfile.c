#include "myfile.h"

static void auxillary_function(void)
{
	printf("Functions that are not intended to be user interface,");
	printf("or not significant enough to be subject to unit tests");
	printf("should be defined as static");
}

int smp_object_action(void)
{
	auxillary_function();
	
	return 42;
}


int smp_object_action2(const struct mol_atom_group* ag)
{
	if (1) {
		return ag->natoms;
	} else {
		return ag->natoms;
	} 
}
