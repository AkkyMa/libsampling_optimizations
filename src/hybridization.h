#ifndef _SMP_HYBRIDIZATION_H_
#define _SMP_HYBRIDIZATION_H_

#include <mol2/atom_group.h>

/**
 * \addtogroup hybridization
 * \brief Module to deal with atom hybridization states.
 *
 * This module provides function \ref smp_hybridization_read to read atom hybridization states from SYBYL (MOL2) files,
 * and some simple wrappers around \c atomgroup metadata functionality.
 *
 * @{
 */

/** Metadata key for accessing hybridizations array */
#define SMP_HYBRIDIZATION_METADATA_KEY "atom_hybridization_state"

enum smp_hybridization_state {
	UNKNOWN_HYBRID = 0,
	SP1_HYBRID,
	SP2_HYBRID,
	SP3_HYBRID,
	RING_HYBRID
};

/**
 * Assign atom hybridization states from SYBYL (MOL2) file.
 * Internally, it is stored as atomgroup's metadata with \ref SMP_HYBRIDIZATION_METADATA_KEY key.
 * NOTE: Functions like \ref copy_atom and \ref mol_atom_group_select are not dealing with metadata, so keep that in mind.
 * \param ag Atomgroup.
 * \param filename Path to the SYBYL (MOL2) file to read.
 * \return True on success, false on failure. The error message is printed to stderr.
 */
bool smp_hybridization_read(struct mol_atom_group *ag, const char *filename);

/**
 * Assign atom hybridization states from libmol JSON file. Only v1 are supported now.
 *
 * Unlike \ref smp_hybridization_read, this function can also read hbond properties of the atoms.
 * But beware:JSON only contains annotations for acceptors donatable hydrogens.
 * It does not contain data about their base atoms! This function infers the donor atoms from the molecule geometry,
 * but does not set up acceptor bases (does not set \c hbond_base_atom_id for acceptor atoms).
 * The \ref smp_hbond_init_bases does recalculate acceptor bases anyway, so probably this should not be an issue if
 * you just want to use HBond potential after calling this function.
 *
 * \param ag Atomgroup.
 * \param filename Path to JSON file to read.
 * \param read_hprop If \c true, also read donor/acceptor data from JSON.
 * \return True on success, false on failure. The error message is printed to stderr.
 */
bool smp_hybridization_read_json(struct mol_atom_group *ag, const char *filename, bool read_hprop);

/**
 * Get the array of hybridization states for atomgroup.
 * \param ag Atomgroup.
 * \return The pointer to (modifiable) array of atom hybridizations.
 */
enum smp_hybridization_state *smp_hybridization_states_fetch(const struct mol_atom_group *ag);

/**
 * Return true if \p ag has assosciated array of hybridization states.
 */
bool smp_hybridization_states_present(const struct mol_atom_group *ag);

/**
 * Remove (and deallocate) array of hybridizations states assosciated with atomgroup.
 */
void smp_hybridization_states_delete(struct mol_atom_group *ag);

/**
 * Currently, \ref mol_atom_group_join does not handle neither hybridization nor donor/acceptor data at all.
 * So, if you want to join two atomgroups and have their hybridization data joined too, don't forget to call
 * this function after \ref mol_atom_group_join:
 * \code{.c}
 * mol_atom_group *c = mol_atom_group_join(a, b);
 * smp_hybridization_atom_group_join(c, a, b);
 * \endcode
 *
 * This will join \c hbond_prop and \c hbond_base_atom_id fields of atomgroups, as well as their hybridization metadata.
 */
void smp_hybridization_atom_group_join(
	struct mol_atom_group *dst,
	const struct mol_atom_group *src_a,
	const struct mol_atom_group *src_b);

/** @}*/
#endif /* _SMP_HYBRIDIZATION_H_ */
