# libsampling
### A small C library of algorithms and data structures for MC-based molecular sampling. 

---

##Requirements
* [libmol2](https://bitbucket.org/bu-structure/libmol2)
* libjansson
* Check
* Doxygen (for building documentation)
* CMake (version > 3.0)

##Installation
After cloning the repository, do
```bash
$ cd libsampling
$ mkdir build && cd build
$ cmake -DBUILD_TESTS=True -DBUILD_DOCS=True ..
$ make
$ make test && make doc
$ make install
```
---
If you want to customize your build, run `ccmake ..` on the build directory and change the variables. The most common variable you may want to change is the build type - we recommend `Release` for normal use, and `Debug` for development. To run tests when building run `make test` on the build directory.

##Licensing
See `LICENSE.txt` for details.

## Authors
Andrey Alekseenko, Ivan Grebenkin, Dzmitry Padhorny

---
##Coding style
For coding style, refer to the `CONTRIBUTING.md` file.
Please, follow the style strictly.   


